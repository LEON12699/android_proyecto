package modelo;

import com.google.android.gms.maps.model.LatLng;

public class Ruta {
    LatLng latitudes;
    String info ="";
    String descripcion ="";

    public Ruta(LatLng latitudes, String info, String descripcion) {
        this.latitudes = latitudes;
        this.info = info;
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Ruta(LatLng latitudes, String info) {
        this.latitudes = latitudes;
        this.info = info;
    }

    public Ruta(LatLng latitudes) {
        this.latitudes = latitudes;
    }

    public LatLng getLatitudes() {
        return latitudes;
    }

    public void setLatitudes(LatLng latitudes) {
        this.latitudes = latitudes;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public boolean has_info(){
       return this.info!="";

    }
}
