package controlador;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class SingletonAlumnoVolly {
    // patron de diseño
    // singleton

    // disconet el hilo y cerrar archivos

    private RequestQueue queue;
    private Context context;
    private static SingletonAlumnoVolly miInstancia;

    public SingletonAlumnoVolly(Context context) {
        this.context = context;
        // get request queue
        queue = getRequestQueue();
    }


    public RequestQueue getRequestQueue(){
        if(queue == null) {
            queue = Volley.newRequestQueue(context.getApplicationContext());
        }

        return queue;
    }

    public static synchronized SingletonAlumnoVolly getInstance(Context context){
     if(miInstancia==null){
       miInstancia = new SingletonAlumnoVolly(context);
     }

    return miInstancia;}



    public <T> void addToRequestQueue(Request request){
        queue.add(request);

    }

}
