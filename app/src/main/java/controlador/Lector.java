package controlador;

import android.util.Log;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import modelo.Ruta;

public class Lector {
    static List<Ruta> list= new ArrayList<>();

    public Lector() {
    }

    public static List<Ruta> archivo(InputStream is){
       list= new ArrayList<>();
        try {
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String jsonformater = new String(buffer,"UTF-8");
            list =llenar_lista(jsonformater);
        } catch (IOException e) {
            e.printStackTrace();
        }

    return list;}


    private static List<Ruta>llenar_lista(String Stringjson){
        List<Ruta> lista = new ArrayList<>();
      try{
        JSONObject json = new JSONObject(Stringjson);
        JSONArray ruta = json.getJSONArray("ruta");
        for (int i=0; i<ruta.length();i++){
            JSONObject json_latitud = ruta.getJSONObject(i);
            LatLng latitud = new LatLng(json_latitud.getDouble("lat"),json_latitud.getDouble("lon"));
            Ruta r = new Ruta(latitud);

            if (json_latitud.has("info")) {
               r.setInfo(json_latitud.getString("info"));
               r.setDescripcion(json_latitud.getString("descripcion"));
            }
            lista.add(r);
        }
    } catch (JSONException e) {
        e.printStackTrace();
    }

return lista;}


}
