package controlador;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import modelo.Producto;

public class HelperProducto extends SQLiteOpenHelper {

    public List<Producto> buscar_codigo(String codigo){
        List<Producto> lista = new ArrayList<>();
        Cursor cursor = this.getReadableDatabase().rawQuery("select * from producto where codigo="+codigo,null);
        if (cursor.moveToFirst()){
            do{
                Producto p = new Producto();
                p.setCodigo(cursor.getInt(cursor.getColumnIndex("codigo")));
                p.setCantidad(cursor.getInt(cursor.getColumnIndex("cantidad")));
                p.setDescripcion(cursor.getString(cursor.getColumnIndex("descripcion")));
                p.setPrecio(cursor.getDouble(cursor.getColumnIndex("precio")));
                lista.add(p);
            }while (cursor.moveToNext());
        }
        cursor.close();
        return lista;

    }

    public void eliminar (){
        this.getWritableDatabase().delete("producto",null,null);

    }
    public void eliminar (Producto producto){
        this.getWritableDatabase().delete("producto","codigo ="+producto.getCodigo(),null);

    }
    public void modificar (Producto producto){
        ContentValues valores = new ContentValues();
        valores.put("codigo", producto.getCodigo());
        valores.put("descripcion", producto.getDescripcion());
        valores.put("cantidad", producto.getCantidad());
        valores.put("precio", producto.getPrecio());
        this.getWritableDatabase().update("producto",valores,"codigo ="+producto.getCodigo(),null) ;// si es string poner entre comillas simples el valor
    }

    public void insertar(Producto producto){
        ContentValues valores = new ContentValues();
        valores.put("codigo", producto.getCodigo());
        valores.put("descripcion", producto.getDescripcion());
        valores.put("cantidad", producto.getCantidad());
        valores.put("precio", producto.getPrecio());
        this.getWritableDatabase().insert("producto",null,valores);
    }

public List<Producto>getProductos(){
    List<Producto> lista = new ArrayList<Producto>();
    Cursor cursor = this.getReadableDatabase().rawQuery("Select * from producto ",null);
    if (cursor.moveToFirst()){
        do{
            Producto p = new Producto();
            p.setCodigo(cursor.getInt(cursor.getColumnIndex("codigo")));
            p.setCantidad(cursor.getInt(cursor.getColumnIndex("cantidad")));
            p.setDescripcion(cursor.getString(cursor.getColumnIndex("descripcion")));
            p.setPrecio(cursor.getDouble(cursor.getColumnIndex("precio")));
            lista.add(p);
        }while (cursor.moveToNext());
    }
    cursor.close();
    return lista;
}


    public HelperProducto(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }


    public HelperProducto(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version, @Nullable DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }




    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table producto(id INTEGER PRIMARY KEY AUTOINCREMENT, descripcion text ,cantidad int(10),precio double(10,2), codigo int(19))");
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
