package com.example.leonisrael.modelo.config;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.app.Application;




public class appORM extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);
    }

}
