package com.example.leonisrael.modelo;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;

import java.util.List;

@Table(name = "carro")
public class carro extends Model {

    @Column(name = "placa", unique = true)
    private String placa;

    @Column(name = "modelo", notNull = true)
    private String modelo;

    @Column(name = "marca", notNull = true)
    private String marca;

    @Column(name = "year", notNull = true)
    private int year;

    public carro(){
    }

    public carro(String placa, String modelo, String marca, int year) {
        super();
        this.placa = placa;
        this.modelo = modelo;
        this.marca = marca;
        this.year = year;
    }



    public String getPlaca() {
        return placa;
    }

    public void setMlaca(String mlaca) {
        this.placa = mlaca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public static List<carro> getAllCarro(){
        return new Select().from(carro.class).execute();
    }
    public static List<carro> deleteAll(){
        return new Delete().from(carro.class).execute();

    }
    public static boolean elimina_placa(String placa){
        getCarroPlaca(placa).delete();
        return true;
    }

    public static carro getCarroPlaca(String placa){
        return new Select().from(carro.class).where("placa = ?", placa).executeSingle();
    }

    public static boolean actualizar(String placa,String modelo,int year, String marca){
        new Update(carro.class).set("placa=?,modelo=?,year=?,marca=?",placa,modelo,year,marca).where("placa=?",placa).execute();
        return true;
    }
}
