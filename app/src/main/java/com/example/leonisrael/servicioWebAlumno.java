package com.example.leonisrael;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;


import modelo.alumno;
import vista.actividades.RecyclerViewOnItemClickListener;
import vista.adapter.alumnoAdapter;




public class servicioWebAlumno extends AppCompatActivity implements View.OnClickListener {
    EditText c_nombre,c_id,c_direccion;
    RecyclerView recycler;
    TextView c_datos;
    Button btn_guardar, btn_actualizar,btn_eliminar,btn_buscarTodo,btn_Buscar_id;
    ServicioWeb servicio;
    alumnoAdapter adapter;
    List<alumno> lista;


    // deifnicion de url del servicio web
    String host="http://reneguaman.000webhostapp.com";
    String insert ="/insertar_alumno.php";
    String get ="/obtener_alumnos.php";
    String update = "/actualizar_alumno.php";
    String delete = "/borrar_alumno.php";
    String getById="/obtener_alumno_por_id.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicio_web_alumno);
        cargar();
    }

    // hilos sicrionos con conexion
    class ServicioWeb extends AsyncTask<String,Void,String>{

        @Override
        protected String doInBackground(String... strings) {

            String consulta ="";
            URL url = null;
            String ruta =strings[0]; // esta es la ruta de obtener alumanos.php
            try {
            if(strings[1].equals("1")){

                    url= new URL(ruta);
                    HttpURLConnection conexion= (HttpURLConnection)url.openConnection();
                    int codigoRespuesta = conexion.getResponseCode();
                    //Log.e("da",""+codigoRespuesta);
                    if(codigoRespuesta==HttpURLConnection.HTTP_OK){
                        InputStream in = new BufferedInputStream(conexion.getInputStream());
                        BufferedReader bf = new BufferedReader(new InputStreamReader(in));
                        consulta+= bf.readLine();

                    }



            }else if(strings[1].equals("2")){
                url = new URL(ruta);
                URLConnection conexion= (URLConnection)url.openConnection();

                    conexion.setDoInput(true);
                    conexion.setDoOutput(true);
                    conexion.setUseCaches(false);
                    conexion.connect();

                    JSONObject objeto = new JSONObject();

                    objeto.put("nombre",strings[2]);
                    objeto.put("direccion",strings[3]);
                    Log.e("objeto",objeto.toString());

                    OutputStream os = conexion.getOutputStream();

                    BufferedWriter bf = new BufferedWriter(new OutputStreamWriter(os,"UTF-8"));
                    bf.write(objeto.toString());
                    bf.flush();
                    bf.close();
                    Log.e("CODE",((HttpURLConnection)conexion).getResponseCode()+"");
                Log.e("MESSAGE",((HttpURLConnection)conexion).getResponseMessage()+"");



            }else if(strings[1].equals("3")){
                url= new URL(ruta);
                HttpURLConnection conexion= (HttpURLConnection)url.openConnection();
                int codigoRespuesta = conexion.getResponseCode();
                //Log.e("da",""+codigoRespuesta);
                if(codigoRespuesta==HttpURLConnection.HTTP_OK){
                    InputStream in = new BufferedInputStream(conexion.getInputStream());
                    BufferedReader bf = new BufferedReader(new InputStreamReader(in));
                    consulta+= bf.readLine();

                }
              }
            else if(strings[1].equals("4")){
                url = new URL(ruta);
                URLConnection conexion= (URLConnection)url.openConnection();

                conexion.setDoInput(true);
                conexion.setDoOutput(true);
                conexion.setUseCaches(false);
                conexion.connect();
                JSONObject objeto = new JSONObject();
                objeto.put("idalumno",strings[2]);

                Log.e("objeto",objeto.toString());

                OutputStream os = conexion.getOutputStream();

                BufferedWriter bf = new BufferedWriter(new OutputStreamWriter(os,"UTF-8"));
                bf.write(objeto.toString());
                bf.flush();
                bf.close();

                if(((HttpURLConnection)conexion).getResponseCode()==HttpURLConnection.HTTP_OK){
                   //Log.e("hola","entro");
                    InputStream in = new BufferedInputStream(conexion.getInputStream());
                    BufferedReader bf4 = new BufferedReader(new InputStreamReader(in));
                    consulta+= bf4.readLine();
                  //  Log.e("COnsulta",consulta);
                }


                Log.e("MESSAGE",((HttpURLConnection)conexion).getResponseMessage()+"");



            }
            else if(strings[1].equals("5")){
                url = new URL(ruta);
                URLConnection conexion= (URLConnection)url.openConnection();

                conexion.setDoInput(true);
                conexion.setDoOutput(true);
                conexion.setUseCaches(false);
                conexion.connect();
                JSONObject objeto = new JSONObject();
                objeto.put("idalumno",strings[4]);
                objeto.put("nombre",strings[2]);
                objeto.put("direccion",strings[3]);
                Log.e("objeto",objeto.toString());

                OutputStream os = conexion.getOutputStream();

                BufferedWriter bf = new BufferedWriter(new OutputStreamWriter(os,"UTF-8"));
                bf.write(objeto.toString());
                bf.flush();
                bf.close();
                if(((HttpURLConnection)conexion).getResponseCode()==HttpURLConnection.HTTP_OK){

                    //Log.e("hola","entro");t
                   /* este metodo carga directamente el nuevo actualizado alumno
                   URL url2= new URL(host.concat(getById).concat("?idalumno="+strings[4]));
                    HttpURLConnection conexion2= (HttpURLConnection)url2.openConnection();
                    int codigoRespuesta2 = conexion2.getResponseCode();
                    Log.e("da",""+codigoRespuesta2);
                    if(codigoRespuesta2==HttpURLConnection.HTTP_OK){
                        InputStream in2 = new BufferedInputStream(conexion2.getInputStream());
                        BufferedReader bf6 = new BufferedReader(new InputStreamReader(in2));
                        consulta+= bf6.readLine();
                        Log.e("consulta",consulta);
                    }*/
                    InputStream in = new BufferedInputStream(conexion.getInputStream());
                    BufferedReader bf4 = new BufferedReader(new InputStreamReader(in));
                      consulta+= bf4.readLine();
                      Log.e("COnsulta",consulta);
                }

                Log.e("CODE",((HttpURLConnection)conexion).getResponseCode()+"");
                Log.e("MESSAGE",((HttpURLConnection)conexion).getResponseMessage()+"");



            }


            } catch (Exception e) {
                e.printStackTrace();
            }


        return consulta;}

        @Override
        protected void onPostExecute(String s) {
            Log.e("data:",s);
            if (s != "") {
                Leer(s);
            }

           //kl c_datos.setText(s);

        }

        public void Leer(String datos){
            Log.e("TAG",datos);
            try {
                lista = new ArrayList<>();
                JSONObject object = new JSONObject(datos);
                if (object.has("alumno")){
                    JSONObject alumno = object.getJSONObject("alumno");
                    alumno a = new alumno();
                    a.setId(alumno.getInt("idAlumno"));
                    a.setDireccion(alumno.getString("direccion"));
                    a.setNombre(alumno.getString("nombre"));
                    lista.add(a);
                }
                else if(object.has("alumnos")){
                JSONArray arr = object.optJSONArray("alumnos");
              //  Log.e("arreglo",arr.getString(0));
                for (int i = 0; i <arr.length() ; i++) {
                    alumno a = new alumno();
                    a.setId(arr.getJSONObject(i).getInt("idalumno"));
                    a.setDireccion(arr.getJSONObject(i).getString("direccion"));
                    a.setNombre(arr.getJSONObject(i).getString("nombre"));
                    lista.add(a);

                }
                }else
                {
                    Toast.makeText(getApplicationContext(),object.getString("mensaje"),Toast.LENGTH_LONG).show();
                }
                carga_recycler();
            }catch (Exception e){
               e.printStackTrace();
               Log.e("eroor",e.toString());
            }
        }
    }


    public void cargar(){
        c_nombre = findViewById(R.id.txt_nombre_alumno);
        c_datos = findViewById(R.id.txt_datos_alumnos);
        c_id = findViewById(R.id.txt_id_alumno);
        c_direccion= findViewById(R.id.txt_direccion_alumno);
        recycler = findViewById(R.id.recycler_alumnos);
        btn_actualizar= findViewById(R.id.btn_actualizar_alumno);
        btn_guardar= findViewById(R.id.btn_guardar_alumno);
        btn_buscarTodo= findViewById(R.id.btn_buscar_alumnos);
        btn_eliminar= findViewById(R.id.btn_eliminar_alumnos);
        btn_Buscar_id=findViewById(R.id.btn_obtener_id_alumno);

        btn_actualizar.setOnClickListener(this);
        btn_guardar.setOnClickListener(this);
        btn_actualizar.setOnClickListener(this);
        btn_Buscar_id.setOnClickListener(this);
        btn_eliminar.setOnClickListener(this);
        btn_buscarTodo.setOnClickListener(this);

    }

    public void carga_recycler(){
      adapter = new alumnoAdapter(lista, new RecyclerViewOnItemClickListener() {
          @Override
          public void onClick(View v, int posicion) {
             TextView nombre =  v.findViewById(R.id.txt_recycler_nombrealumno);
            TextView direccion = v.findViewById(R.id.txt_direccionalumno);
            TextView id = v.findViewById(R.id.txt_recycler_alumnoid);

            c_nombre.setText(nombre.getText().toString());
            c_id.setText(id.getText().toString());
            c_direccion.setText(direccion.getText().toString());
          }
      });
        recycler.setLayoutManager(new LinearLayoutManager(this));

        DividerItemDecoration divider = new DividerItemDecoration(recycler.getContext(),((LinearLayoutManager)recycler.getLayoutManager()).getOrientation());
        recycler.addItemDecoration(divider);
        recycler.setAdapter(adapter);

    }

    @Override
    public void onClick(View v) {
        servicio = new ServicioWeb();
        switch(v.getId()){
            case R.id.btn_guardar_alumno:
                servicio.execute(host.concat(insert),"2",c_nombre.getText().toString(),c_direccion.getText().toString());
                break;
            case R.id.btn_eliminar_alumnos:
                servicio.execute(host.concat(delete),"4",c_id.getText().toString());
                break;
            case R.id.btn_actualizar_alumno:
                servicio.execute(host.concat(update),"5",c_nombre.getText().toString(),c_direccion.getText().toString(),c_id.getText().toString());

                break;
            case R.id.btn_buscar_alumnos:
              //  Toast.makeText(this,host.concat(get),1).show();
               servicio.execute(host.concat(get),"1"); //ejecuta el hilo en el doInbackground

                break;
            case R.id.btn_obtener_id_alumno:
                servicio.execute(host.concat(getById).concat("?idalumno="+c_id.getText().toString()),"3");
                break;

        }
    }
}
