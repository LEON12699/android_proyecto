package com.example.leonisrael.Estudiante_volly;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.leonisrael.Estudiante_volly.modelos.Estudiante;

import org.json.JSONException;
import org.json.JSONObject;

import controlador.SingletonAlumnoVolly;
import controlador.volly_async;

public class Servicio_estudiante_volley {
String ip = "192.168.1.12";
    //String host="http://"+ip+"/archivos/LEONISRAEL";
    String host="http://192.168.1.12/archivos/LEONISRAEL";
    String GET="/wsJSONConsultarLista.php";
    String INSERT="/wsJSONRegistroMovil.php";
    String UPDATE="/wsJSONUpdateMovil.php";
    String BORRAR="/wsJSONDeleteMovil.php";
    String OBTENERID="/wsJSONConsultarUsuarioUrl.php";
    Context context;

    public Servicio_estudiante_volley(Context context) {
        this.context = context;
    }

    public void FindAll(final volly_async voly){
        String path =host.concat(GET);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, path, null,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Toast.makeText(context, response.toString(), Toast.LENGTH_LONG).show();
                voly.response(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        SingletonAlumnoVolly.getInstance(context).addToRequestQueue(request);
    }

    public void actualizar(Estudiante e ,final  volly_async voly){
        String path=host.concat(UPDATE);
        JSONObject json = new JSONObject();
        try {
            json.put("documento",e.getDocumemto());
            json.put("nombre",e.getNombre());
            json.put("profesion",e.getProfesion());
            json.put("imagen","");
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, path,json,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            voly.response(response);

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            SingletonAlumnoVolly.getInstance(context).addToRequestQueue(request);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }


    public void obtenerId(String documento,final volly_async voly){
        String path = host.concat(OBTENERID).concat("?documento="+documento);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, path, null,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Toast.makeText(context, response.toString(), Toast.LENGTH_LONG).show();
                voly.response(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        SingletonAlumnoVolly.getInstance(context).addToRequestQueue(request);
    }


    public void eliminar(String documento,final volly_async voly){
        String path = host.concat(BORRAR).concat("?documento="+documento);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, path, null,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Toast.makeText(context, response.toString(), Toast.LENGTH_LONG).show();
                voly.response(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        SingletonAlumnoVolly.getInstance(context).addToRequestQueue(request);

    }


    public void Insertar(Estudiante e,final volly_async voly){
        String path=host.concat(INSERT);
        JSONObject json = new JSONObject();
        try {
            json.put("documento",e.getDocumemto());
            json.put("nombre",e.getNombre());
            json.put("profesion",e.getProfesion());
            json.put("imagen","");
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, path,json,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            voly.response(response);

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            SingletonAlumnoVolly.getInstance(context).addToRequestQueue(request);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }


    }

}
