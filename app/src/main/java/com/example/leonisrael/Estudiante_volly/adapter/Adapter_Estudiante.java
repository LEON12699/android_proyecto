package com.example.leonisrael.Estudiante_volly.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.leonisrael.Estudiante_volly.modelos.Estudiante;
import com.example.leonisrael.R;

import java.util.List;

import modelo.alumno;
import vista.actividades.RecyclerViewOnItemClickListener;

public class Adapter_Estudiante extends RecyclerView.Adapter<Adapter_Estudiante.ViewHolderestudiante> {
    private RecyclerViewOnItemClickListener recyclerViewOnItemClickListener;
    private List<Estudiante> lista;

    public Adapter_Estudiante(RecyclerViewOnItemClickListener recyclerViewOnItemClickListener, List<Estudiante> lista) {
        this.recyclerViewOnItemClickListener = recyclerViewOnItemClickListener;
        this.lista = lista;
    }

    @NonNull
    @Override
    public Adapter_Estudiante.ViewHolderestudiante onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_estudiante_card,parent,false);

        return new Adapter_Estudiante.ViewHolderestudiante(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Adapter_Estudiante.ViewHolderestudiante holder, int position) {
        holder.nombre.setText(lista.get(position).getNombre());
        holder.id.setText(lista.get(position).getDocumemto());
        holder.profesion.setText(lista.get(position).getProfesion());
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class ViewHolderestudiante extends RecyclerView.ViewHolder implements View.OnClickListener{
            TextView nombre,profesion,id;
        public ViewHolderestudiante(@NonNull View itemView) {
            super(itemView);
            nombre= itemView.findViewById(R.id.txt_enombre_itemcard);
            profesion=itemView.findViewById(R.id.txt_eprofesion_itemcard);
            id=itemView.findViewById(R.id.txt_eid_itemcard);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            recyclerViewOnItemClickListener.onClick(v,getAdapterPosition());
        }
    }
}
