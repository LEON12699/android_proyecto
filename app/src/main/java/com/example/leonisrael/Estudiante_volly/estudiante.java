package com.example.leonisrael.Estudiante_volly;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.leonisrael.Estudiante_volly.adapter.Adapter_Estudiante;
import com.example.leonisrael.Estudiante_volly.modelos.Estudiante;
import com.example.leonisrael.R;
import com.example.leonisrael.servicioWebAlumno;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import controlador.volly_async;
import modelo.alumno;
import vista.actividades.RecyclerViewOnItemClickListener;
import vista.adapter.alumnoAdapter;

import static com.activeandroid.Cache.getContext;

public class estudiante extends AppCompatActivity implements View.OnClickListener{

    EditText c_nombre,c_id,c_profesion;
    RecyclerView recycler;
    Button btn_guardar, btn_actualizar,btn_eliminar,btn_buscarTodo,btn_Buscar_id;
    Servicio_estudiante_volley servicio = new Servicio_estudiante_volley(this);
    Adapter_Estudiante adapter;
    List<Estudiante> lista;

    public  void cargar(){
        c_nombre = findViewById(R.id.txt_SW_nombre_estudiante);
        c_id = findViewById(R.id.txt_SW_id_estudiante);
        c_profesion= findViewById(R.id.txt_SW_profesion_estudiante);
        recycler = findViewById(R.id.SW_recycler_estudiante);
        btn_actualizar= findViewById(R.id.btn_SW_actualizar_estudiante);
        btn_guardar= findViewById(R.id.btn_SW_guardar_estudiante);
        btn_buscarTodo= findViewById(R.id.btn_SW_buscar_estudiantes);
        btn_eliminar= findViewById(R.id.btn_SW_eliminar_estudiantes);
        btn_Buscar_id=findViewById(R.id.btn_SW_obtener_id_estudiante);

        btn_actualizar.setOnClickListener(this);
        btn_guardar.setOnClickListener(this);
        btn_actualizar.setOnClickListener(this);
        btn_Buscar_id.setOnClickListener(this);
        btn_eliminar.setOnClickListener(this);
        btn_buscarTodo.setOnClickListener(this);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estudiante);
        cargar();

    }

    public void carga_recycler(){
        adapter = new Adapter_Estudiante(new RecyclerViewOnItemClickListener() {
                    @Override
                    public void onClick(View v, int posicion) {

                        TextView documento =  v.findViewById(R.id.txt_eid_itemcard);
                        TextView nombre = v.findViewById(R.id.txt_enombre_itemcard);
                        TextView profesion = v.findViewById(R.id.txt_eprofesion_itemcard);

                        c_nombre.setText(nombre.getText().toString());
                        c_id.setText(documento.getText().toString());
                        c_profesion.setText(profesion.getText().toString());
                    }
                }, lista);
       // recycler.setLayoutManager(new LinearLayoutManager(this).anim);
       // recycler.se
     //   DividerItemDecoration divider = new DividerItemDecoration(recycler.getContext(),((LinearLayoutManager)recycler.getLayoutManager()).getOrientation());
     //   recycler.addItemDecoration(divider);

        recycler.setAdapter(adapter);

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
        case R.id.btn_SW_buscar_estudiantes:
            final Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.dialog_progress);
            dialog.setTitle("Progress");
            dialog.show();
            servicio.FindAll(new volly_async() {
                @Override
                public void response(JSONObject json)  {
                    JSONObject objeto = json;
                    JSONArray arr = objeto.optJSONArray("usuario");
                    lista=new ArrayList<>();

                    //  Log.e("arreglo",arr.getString(0));
                    for (int i = 0; i <arr.length() ; i++) {
                        try {
                        Estudiante a = new Estudiante();
                        a.setDocumemto(arr.getJSONObject(i).getString("documento"));
                        a.setProfesion(arr.getJSONObject(i).getString("profesion"));
                        a.setNombre(arr.getJSONObject(i).getString("nombre"));
                            lista.add(a);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                    dialog.hide();
                    carga_recycler();
                }
            });
            break;
            case R.id.btn_SW_guardar_estudiante:
                final Estudiante student = new Estudiante();
                student.setDocumemto(c_id.getText().toString());
                student.setProfesion(c_profesion.getText().toString());
                student.setNombre(c_nombre.getText().toString());
                servicio.Insertar(student, new volly_async() {
                    @Override
                    public void response(JSONObject respuesta) {

                        Log.e("err","salio");
                        lista = new ArrayList<>();
                        lista.add(student);

                        try {
                        String mensaje = respuesta.getString("mensaje");
                        Toast.makeText(getApplicationContext(),mensaje,Toast.LENGTH_LONG).show();
                        carga_recycler();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.e("ERROR",respuesta.toString());

                    }
                });
                break;
            case R.id.btn_SW_eliminar_estudiantes:
                servicio.eliminar(c_id.getText().toString(), new volly_async() {
                    @Override
                    public void response(JSONObject json) {
                        lista= new ArrayList<>();

                        try {
                            String mensaje = json.getString("mensaje");
                            Toast.makeText(getApplicationContext(),mensaje,Toast.LENGTH_LONG).show();
                            carga_recycler();
                            }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });
                break;
            case R.id.btn_SW_actualizar_estudiante:
                final Estudiante studenta = new Estudiante();
                studenta.setDocumemto(c_id.getText().toString());
                studenta.setProfesion(c_profesion.getText().toString());
                studenta.setNombre(c_nombre.getText().toString());

                servicio.actualizar(studenta, new volly_async() {
                    @Override
                    public void response(JSONObject json) {
                        lista = new ArrayList<>();
                        lista.add(studenta);

                        try {
                            String mensaje = json.getString("mensaje");
                            Toast.makeText(getApplicationContext(),mensaje,Toast.LENGTH_LONG).show();
                            carga_recycler();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                break;
            case R.id.btn_SW_obtener_id_estudiante:
                servicio.obtenerId(c_id.getText().toString(), new volly_async() {
                    @Override
                    public void response(JSONObject json) {
                        JSONObject objeto = json;
                        JSONArray arr = objeto.optJSONArray("usuario");
                        lista=new ArrayList<>();

                        //  Log.e("arreglo",arr.getString(0));
                        for (int i = 0; i <arr.length() ; i++) {
                            try {
                                Estudiante a = new Estudiante();
                                a.setDocumemto(arr.getJSONObject(i).getString("documento"));
                                a.setProfesion(arr.getJSONObject(i).getString("profesion"));
                                a.setNombre(arr.getJSONObject(i).getString("nombre"));
                                lista.add(a);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                        carga_recycler();
                    }
                });
                break;
        }
    }
}
