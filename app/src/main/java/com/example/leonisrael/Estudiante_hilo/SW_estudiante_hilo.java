package com.example.leonisrael.Estudiante_hilo;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class SW_estudiante_hilo extends AsyncTask<String,Void,JSONObject> {


    @Override
    protected JSONObject doInBackground(String... strings) {
        JSONObject consulta =null;
        URL url = null;
        String ruta =strings[0];
        if(strings[1].equals("1")){
            try {
                url = new URL(ruta);
                HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                int codigoRespuesta = conexion.getResponseCode();
                //Log.e("da",""+codigoRespuesta);
                if (codigoRespuesta == HttpURLConnection.HTTP_OK) {
                    InputStream in = new BufferedInputStream(conexion.getInputStream());
                    BufferedReader bf = new BufferedReader(new InputStreamReader(in));
                    StringBuilder sb = new StringBuilder();
                    String l = null;
                    while ((l = bf.readLine()) != null) {
                        sb.append(l + "\n");
                    }
                    consulta = new JSONObject(sb.toString());
                }
            }catch (IOException | JSONException e) {
                    e.printStackTrace();
                }


        }else if(strings[1].equals("2")) {
            try {
                url = new URL(ruta.concat("?documento=" + strings[2]));
                HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                int codigoRespuesta = conexion.getResponseCode();
                //Log.e("da",""+codigoRespuesta);
                if (codigoRespuesta == HttpURLConnection.HTTP_OK) {
                    InputStream in = new BufferedInputStream(conexion.getInputStream());
                    BufferedReader bf = new BufferedReader(new InputStreamReader(in));
                    StringBuilder sb = new StringBuilder();
                    String l = null;
                    while ((l = bf.readLine()) != null) {
                        sb.append(l + "\n");
                    }
                    consulta = new JSONObject(sb.toString());
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
        }
            else if(strings[1].equals("3")){
                try {
                    url = new URL(ruta.concat("?documento="+strings[2]));
                    HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                    int codigoRespuesta = conexion.getResponseCode();
                    //Log.e("da",""+codigoRespuesta);
                    if (codigoRespuesta == HttpURLConnection.HTTP_OK) {
                        InputStream in = new BufferedInputStream(conexion.getInputStream());
                        BufferedReader bf = new BufferedReader(new InputStreamReader(in));
                        StringBuilder sb = new StringBuilder();
                        String l = null;
                        while ((l = bf.readLine()) != null) {
                            sb.append(l + "\n");
                        }
                        consulta = new JSONObject(sb.toString());
                    }
                }catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
        }else if(strings[1].equals("4")){
            try {
                url = new URL(ruta);
            URLConnection conexion= (URLConnection)url.openConnection();
            conexion.setDoInput(true);
            conexion.setDoOutput(true);
            conexion.setUseCaches(false);
            conexion.setRequestProperty("Content-Type", "application/json");
            ((HttpURLConnection)conexion).setRequestMethod("POST");
            conexion.connect();

            JSONObject objeto = new JSONObject();
            objeto.put("documento",strings[2]);
            objeto.put("nombre",strings[3]);
            objeto.put("profesion",strings[4]);
            objeto.put("imagen","");
                OutputStream os = conexion.getOutputStream();

                BufferedWriter bf = new BufferedWriter(new OutputStreamWriter(os,"UTF-8"));
                bf.write(objeto.toString());
                Log.e("bf",objeto.toString());
                bf.flush();
                bf.close();
                os.close();

                if(((HttpURLConnection)conexion).getResponseCode()==HttpURLConnection.HTTP_OK){
                    //Log.e("hola","entro");
                    InputStream in = new BufferedInputStream(conexion.getInputStream());
                    BufferedReader bfinsert = new BufferedReader(new InputStreamReader(in));
                    StringBuilder sb = new StringBuilder();
                    String lector = null;
                    while ((lector = bfinsert.readLine()) != null) {
                        sb.append(lector + "\n");
                    }
                    consulta = new JSONObject(sb.toString());
                    //  Log.e("COnsulta",consulta);
                }

            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }

        }else if(strings[1].equals("5")){
            try {
                url = new URL(ruta);
                URLConnection conexion= (URLConnection)url.openConnection();
                ((HttpURLConnection)conexion).setRequestMethod("POST");
                conexion.setDoInput(true);
                conexion.setDoOutput(true);
                conexion.setUseCaches(false);
                conexion.setRequestProperty("Content-Type", "application/json");
                conexion.connect();

                JSONObject objeto = new JSONObject();
                objeto.put("documento",strings[2]);
                objeto.put("nombre",strings[3]);
                objeto.put("profesion",strings[4]);
                objeto.put("imagen","");
                OutputStream os = conexion.getOutputStream();

                BufferedWriter bf = new BufferedWriter(new OutputStreamWriter(os,"UTF-8"));
                bf.write(objeto.toString());
                bf.flush();
                bf.close();
                os.close();

                if(((HttpURLConnection)conexion).getResponseCode()==HttpURLConnection.HTTP_OK){
                    //Log.e("hola","entro");
                    InputStream in = new BufferedInputStream(conexion.getInputStream());
                    BufferedReader bfinsert = new BufferedReader(new InputStreamReader(in));
                    StringBuilder sb = new StringBuilder();
                    String lector = null;
                    while ((lector = bfinsert.readLine()) != null) {
                        sb.append(lector + "\n");
                    }
                    consulta = new JSONObject(sb.toString());
                    //  Log.e("COnsulta",consulta);
                }

            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
        }

        return consulta;
    }
}
