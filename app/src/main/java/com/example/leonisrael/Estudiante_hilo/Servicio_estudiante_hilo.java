package com.example.leonisrael.Estudiante_hilo;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.leonisrael.Estudiante_volly.Servicio_estudiante_volley;
import com.example.leonisrael.Estudiante_volly.adapter.Adapter_Estudiante;
import com.example.leonisrael.Estudiante_volly.modelos.Estudiante;
import com.example.leonisrael.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import vista.actividades.RecyclerViewOnItemClickListener;

import static android.provider.ContactsContract.CommonDataKinds.Website.URL;

public class Servicio_estudiante_hilo extends AppCompatActivity implements View.OnClickListener {
    EditText c_nombre,c_id,c_profesion;
    RecyclerView recycler;
    Button btn_guardar, btn_actualizar,btn_eliminar,btn_buscarTodo,btn_Buscar_id;
    ImageButton pick;
    SW_estudiante_hilo servicio ;
    Adapter_Estudiante adapter;
    List<Estudiante> lista;
    Uri URL;

    String host="http://192.168.1.12/archivos/LEONISRAEL";
    String GET="/wsJSONConsultarLista.php";
    String INSERT="/wsJSONRegistroMovil.php";
    String UPDATE="/wsJSONUpdateMovil.php";
    String BORRAR="/wsJSONDeleteMovil.php";
    String OBTENERID="/wsJSONConsultarUsuarioUrl.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicio_estudiante_hilo);
        cargar();
    }

    public  void cargar(){
        c_nombre = findViewById(R.id.txt_SWHILO_nombre_estudiante);
        c_id = findViewById(R.id.txt_SWHILO_id_estudiante);
        c_profesion= findViewById(R.id.txt_SWHILO_profesion_estudiante);
        recycler = findViewById(R.id.SWHILO_recycler_estudiante);
        btn_actualizar= findViewById(R.id.btn_SWHILO_actualizar_estudiante);
        btn_guardar= findViewById(R.id.btn_SWHILO_guardar_estudiante);
        btn_buscarTodo= findViewById(R.id.btn_SWHILO_buscar_estudiantes);
        btn_eliminar= findViewById(R.id.btn_SWHILO_eliminar_estudiantes);
        btn_Buscar_id=findViewById(R.id.btn_SWHILO_obtener_id_estudiante);
        pick= findViewById(R.id.foto_estudiante);

        pick.setOnClickListener(this);

        btn_actualizar.setOnClickListener(this);
        btn_guardar.setOnClickListener(this);
        btn_actualizar.setOnClickListener(this);
        btn_Buscar_id.setOnClickListener(this);
        btn_eliminar.setOnClickListener(this);
        btn_buscarTodo.setOnClickListener(this);
    }

    public void Cargar_lista(JSONObject objeto){
        JSONArray arr = objeto.optJSONArray("usuario");
        lista=new ArrayList<>();

        //  Log.e("arreglo",arr.getString(0));
        for (int i = 0; i <arr.length() ; i++) {
            try {
                Estudiante a = new Estudiante();
                a.setDocumemto(arr.getJSONObject(i).getString("documento"));
                a.setProfesion(arr.getJSONObject(i).getString("profesion"));
                a.setNombre(arr.getJSONObject(i).getString("nombre"));
                lista.add(a);
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }

    public void carga_recycler(){
        adapter = new Adapter_Estudiante(new RecyclerViewOnItemClickListener() {
            @Override
            public void onClick(View v, int posicion) {

                TextView documento =  v.findViewById(R.id.txt_eid_itemcard);
                TextView nombre = v.findViewById(R.id.txt_enombre_itemcard);
                TextView profesion = v.findViewById(R.id.txt_eprofesion_itemcard);

                c_nombre.setText(nombre.getText().toString());
                c_id.setText(documento.getText().toString());
                c_profesion.setText(profesion.getText().toString());
            }
        }, lista);
        // recycler.setLayoutManager(new LinearLayoutManager(this).anim);
        // recycler.se
        //   DividerItemDecoration divider = new DividerItemDecoration(recycler.getContext(),((LinearLayoutManager)recycler.getLayoutManager()).getOrientation());
        //   recycler.addItemDecoration(divider);

        recycler.setAdapter(adapter);

    }

    @Override
    public void onClick(View v) {
        servicio = new SW_estudiante_hilo();
        switch(v.getId()){
            case R.id.btn_SWHILO_buscar_estudiantes:
                try {
                    JSONObject objeto = servicio.execute(host.concat(GET),"1").get();
                    Cargar_lista(objeto);
                    carga_recycler();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btn_SWHILO_obtener_id_estudiante:
                try {
                    JSONObject objeto = servicio.execute(host.concat(OBTENERID),"2",c_id.getText().toString()).get();
                    Cargar_lista(objeto);
                    carga_recycler();
                } catch (ExecutionException | InterruptedException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btn_SWHILO_eliminar_estudiantes:
                try {
                    JSONObject objeto = servicio.execute(host.concat(BORRAR),"3",c_id.getText().toString()).get();
                    Toast.makeText(this,objeto.getString("mensaje"),Toast.LENGTH_LONG).show();
                    lista = new ArrayList<>();
                    carga_recycler();
                } catch (ExecutionException | JSONException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                break;
            case R.id.btn_SWHILO_guardar_estudiante:
                Estudiante student = new Estudiante();
                student.setNombre(c_nombre.getText().toString());
                student.setDocumemto(c_id.getText().toString());
                student.setProfesion(c_profesion.getText().toString());
                try {
                    JSONObject objeto = servicio.execute(host.concat(INSERT),"4",student.getDocumemto(),student.getNombre(),student.getProfesion()).get();
                    if (objeto.has("mensaje")){
                        Toast.makeText(this,objeto.getString("mensaje"),Toast.LENGTH_LONG).show();
                        lista.clear();
                        lista.add(student);
                        carga_recycler();

                    }

                } catch (ExecutionException | JSONException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btn_SWHILO_actualizar_estudiante:
                Estudiante student_a = new Estudiante();
                student_a.setNombre(c_nombre.getText().toString());
                student_a.setDocumemto(c_id.getText().toString());
                student_a.setProfesion(c_profesion.getText().toString());
                try {
                    JSONObject objeto = servicio.execute(host.concat(UPDATE),"5",student_a.getDocumemto(),student_a.getNombre(),student_a.getProfesion()).get();
                    if (objeto.has("mensaje")){
                        Toast.makeText(this,objeto.getString("mensaje"),Toast.LENGTH_LONG).show();
                        lista.clear();
                        lista.add(student_a);
                        carga_recycler();

                    }

                } catch (ExecutionException | JSONException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


                break;
            case R.id.foto_estudiante:
                Intent intent;
                intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(intent.createChooser(intent,"Selecciona Una imagen"),200);
                break;
    }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode== Activity.RESULT_OK) {
            URL = data != null ? data.getData() : null;
            pick.setImageURI(URL);

        }


    }

    /*public void subir(){
        InputStream is = getContentResolver().openInputStream(URL);
        Bitmap bitmap = BitmapFactory.decodeStream(is);
        ByteArrayOutputStream a = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100,a);
        byte[] bytes=a.toByteArray();
        String cadena = Base64.encodeToString(bytes,Base64.DEFAULT);
        bitmap.recycle();

    }*/
}
