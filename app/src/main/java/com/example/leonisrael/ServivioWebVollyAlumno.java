package com.example.leonisrael;

import android.content.Context;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;

import controlador.SingletonAlumnoVolly;
import controlador.volly_async;
import modelo.alumno;

public class ServivioWebVollyAlumno {

    String host="http://reneguaman.000webhostapp.com";
    String insert ="/insertar_alumno.php";
    String get ="/obtener_alumnos.php";
    String update = "/actualizar_alumno.php";
    String delete = "/borrar_alumno.php";
    String getById="/obtener_alumno_por_id.php";


    JSONObject consulta;
    Context context;
    boolean estado ;



    public ServivioWebVollyAlumno(Context context) {
        this.context = context;

    }

    public JSONObject findAllStudents()
    {
        String path = host.concat(get) ;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, path, null,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Toast.makeText(context, response.toString(), Toast.LENGTH_LONG).show();
                consulta=response;

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });


        SingletonAlumnoVolly.getInstance(context).addToRequestQueue(request);


        return consulta;
    }

    public JSONObject InsertStudent(alumno alumno){
        String path= host.concat(insert);
        JSONObject json = new JSONObject();

        try {
            json.put("nombre",alumno.getNombre());
            json.put("direccion",alumno.getDireccion());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, path,json,
                new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                estado = true;
                try {
                    consulta=response;
                    Log.e("response",response.toString());
                    Toast.makeText(context,response.getString("estado"),Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        SingletonAlumnoVolly.getInstance(context).addToRequestQueue(request);

        return consulta;
    }
    public void findAllStudents(final volly_async voly)
    {
        String path = host.concat(get) ;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, path, null,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Toast.makeText(context, response.toString(), Toast.LENGTH_LONG).show();

                voly.response(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        SingletonAlumnoVolly.getInstance(context).addToRequestQueue(request);

    }

    public void  InsertStudent(alumno alumno, final volly_async async){
        String path= host.concat(insert);
        JSONObject json = new JSONObject();
        try {
            json.put("nombre",alumno.getNombre());
            json.put("direccion",alumno.getDireccion());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, path,json,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        async.response(response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        SingletonAlumnoVolly.getInstance(context).addToRequestQueue(request);
    }


    public void DeleteIdStudent(alumno alumno,final  volly_async async){
        String path= host.concat(delete);
        JSONObject json = new JSONObject();
        try {
            json.put("idalumno",alumno.getId());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, path, json,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        async.response(response);

                    }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        SingletonAlumnoVolly.getInstance(context).addToRequestQueue(request);
    }

    public void UpdateStudent(alumno alumno,final  volly_async async){
        String path= host.concat(update);
        JSONObject json = new JSONObject();
        try {
            json.put("idalumno",alumno.getId());
            json.put("nombre",alumno.getNombre());
            json.put("direccion",alumno.getDireccion());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, path, json,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        async.response(response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        SingletonAlumnoVolly.getInstance(context).addToRequestQueue(request);
    }

    public void getByid(String id,final volly_async async){
        String path= host.concat(getById).concat("?idalumno="+id);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, path, null,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Toast.makeText(context, response.toString(), Toast.LENGTH_LONG).show();
                async.response(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        SingletonAlumnoVolly.getInstance(context).addToRequestQueue(request);

    }

}
