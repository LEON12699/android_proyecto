package com.example.leonisrael;

import androidx.appcompat.app.ActionBar;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import com.example.leonisrael.Estudiante_hilo.SW_estudiante_hilo;
import com.example.leonisrael.Estudiante_hilo.Servicio_estudiante_hilo;
import com.example.leonisrael.Estudiante_volly.estudiante;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import vista.actividades.ActividadProductoHelper;
import vista.actividades.Activity_voly;
import vista.actividades.AppORM;
import vista.actividades.MapsActivity;
import vista.actividades.Servicio_clima;
import vista.actividades.actividad_archivosArtistas;
import vista.actividades.activity_recicler_artistas;
import vista.actividades.leecutra_reyes;
import vista.adapter.ExpandedMenuModel;
import vista.adapter.ExpandibleAdapter;
import vista.fragmentos.ejercicio1;
import vista.actividades.enviarDatos;
import vista.fragmentos.fragmento;
import vista.actividades.login;
import vista.actividades.suma;
import vista.sensor.Sensor_luz;
import vista.sensor.Sensor_proximidad;
import vista.sensor.Sensores;

public class principal extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private ExpandibleAdapter mMenuAdapter;
    private ExpandableListView expandableList;
    List<ExpandedMenuModel> listDataHeader;
    HashMap<ExpandedMenuModel,List<String>> listDataChild;
   private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setHomeAsUpIndicator(android.R.drawable.ic_menu_more);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
           // esta parte solo de prueba borrar luego


            //------------------------------//
            }
        });
        drawer = findViewById(R.id.drawer_layout);
        expandableList = findViewById(R.id.navigationmenu);
        NavigationView navigationView = findViewById(R.id.nav_view);
        if (navigationView!=null){
            setupDrawerContent(navigationView);
        }
        prepareListData();
        mMenuAdapter = new ExpandibleAdapter(this,listDataHeader,listDataChild);

        expandableList.setAdapter(mMenuAdapter);

        expandableList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                Intent intent ;

                if(!listDataChild.get(listDataHeader.get(groupPosition)).isEmpty()){
                switch(listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition)) {
                    case "login":
                        intent = new Intent(getApplicationContext(), login.class);
                        startActivity(intent);
                        break;
                    case "sumar":
                        intent = new Intent(getApplicationContext(), suma.class);
                        startActivity(intent);
                        break;
                    case "parametros":
                        intent = new Intent(getApplicationContext(), enviarDatos.class);
                        startActivity(intent);
                        break;
                    case "colores":
                        intent = new Intent(getApplicationContext(), fragmento.class);
                        startActivity(intent);
                        break;
                    case "datos entre fragmentos":
                        intent = new Intent(getApplicationContext(), ejercicio1.class);
                        startActivity(intent);
                        break;
                    case "dialog sumar":
                        final Dialog dlgSUmar = new Dialog(principal.this);
                        dlgSUmar.setContentView(R.layout.dialog_sumar);
                        Button btn_dialog_sumar = dlgSUmar.findViewById(R.id.dialog_btn_sumar);
                        final EditText caja1 = dlgSUmar.findViewById(R.id.dialog_numero1);
                        final EditText caja2 = dlgSUmar.findViewById(R.id.dialog_numero2);
                        dlgSUmar.show();
                        btn_dialog_sumar.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                double numero = Double.parseDouble(caja1.getText().toString()) + Double.parseDouble(caja2.getText().toString());
                                Toast.makeText(getApplicationContext(), "sumar es " + numero, Toast.LENGTH_SHORT).show();
                                dlgSUmar.hide();

                            }
                        });
                        break;
                    case "recyclerview":
                        intent = new Intent(getApplicationContext(), activity_recicler_artistas.class);
                        startActivity(intent);
                        break;

                    case "reyes":
                        intent = new Intent(getApplicationContext(), leecutra_reyes
                                .class);
                        startActivity(intent);
                        break;
                    case "artistas":
                        intent = new Intent(getApplicationContext(), actividad_archivosArtistas.class);
                        startActivity(intent);
                        break;
                    case "helper":
                        intent = new Intent(getApplicationContext(), ActividadProductoHelper
                                .class);
                        startActivity(intent);
                        break;
                    case "ORM":
                        intent = new Intent(getApplicationContext(), AppORM.class);
                        startActivity(intent);
                        break;
                    case "hilo":
                        intent = new Intent(getApplicationContext(), servicioWebAlumno.class);
                        startActivity(intent);
                        break;
                    case "hilo_weather":
                        intent = new Intent(getApplicationContext(), Servicio_clima.class);
                        startActivity(intent);
                        break;
                    case "volly":
                        intent = new Intent(getApplicationContext(), Activity_voly.class);
                        startActivity(intent);
                        break;
                    case "Estudiante_hilo":
                        intent = new Intent(getApplicationContext(), estudiante.class);
                        startActivity(intent);
                        break;
                    case "Estudiante_voley":
                        intent = new Intent(getApplicationContext(), Servicio_estudiante_hilo.class);
                        startActivity(intent);
                        break;
                    case "Proximidad":
                        intent = new Intent(getApplicationContext(), Sensor_proximidad.class);
                        startActivity(intent);
                        break;
                    case "luz":
                        intent = new Intent(getApplicationContext(), Sensor_luz.class);
                        startActivity(intent);
                        break;
                    case "Acelerometro":
                        intent = new Intent(getApplicationContext(), Sensores.class);
                        startActivity(intent);
                        break;
                    case "Mapas":
                        intent = new Intent(getApplicationContext(),MapsActivity.class);
                        startActivity(intent);
                        break;


                }
              }

                Toast.makeText(getApplicationContext(),childPosition+"",Toast.LENGTH_LONG).show();


               // Toast.makeText(getApplicationContext(),listDataHeader.get(groupPosition)+":"+listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition),Toast.LENGTH_LONG).show();
                return false;
            }
        });
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.

       //mAppBarConfiguration = new AppBarConfiguration.Builder(
         //       R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow,
         //       R.id.nav_tools, R.id.nav_share, R.id.nav_send)
         //       .setDrawerLayout(drawer)
          //      .build();
        //NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
       // NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        //NavigationUI.setupWithNavController(navigationView, navController);
    }


    private void prepareListData(){
        listDataHeader = new ArrayList<ExpandedMenuModel>();
        listDataChild= new HashMap<ExpandedMenuModel,List<String>>();


        ExpandedMenuModel item1 = new ExpandedMenuModel();
        item1.setIconName("Eventos");
        item1.setIconImg(android.R.drawable.ic_menu_add);

        ExpandedMenuModel item2 = new ExpandedMenuModel();
        item2.setIconName("Fragmentos");
        item2.setIconImg(android.R.drawable.ic_menu_add);

        ExpandedMenuModel item3 = new ExpandedMenuModel();
        item3.setIconName("Sensores");
        item3.setIconImg(android.R.drawable.ic_menu_add);

        ExpandedMenuModel item4 = new ExpandedMenuModel();
        item4.setIconName("archivos");
        item4.setIconImg(android.R.drawable.ic_menu_add);

        ExpandedMenuModel item5 = new ExpandedMenuModel();
        item5.setIconName("BD");
        item5.setIconImg(android.R.drawable.ic_menu_add);

        ExpandedMenuModel item6 = new ExpandedMenuModel();
        item6.setIconName("SW");
        item6.setIconImg(android.R.drawable.ic_menu_add);

        ExpandedMenuModel item7 = new ExpandedMenuModel();
        item7.setIconName("Mapas");
        item7.setIconImg(android.R.drawable.ic_menu_add);

        ExpandedMenuModel item8 = new ExpandedMenuModel();
        item8.setIconName("SW estudiante");
        item8.setIconImg(android.R.drawable.ic_menu_add);


        listDataHeader.add(item1);
        listDataHeader.add(item2);
        listDataHeader.add(item3);
        listDataHeader.add(item4);
        listDataHeader.add(item5);
        listDataHeader.add(item6);
        listDataHeader.add(item7);
        listDataHeader.add(item8);

        List<String> heading1 = new ArrayList<String>();
        heading1.add("login");
        heading1.add("sumar");
        heading1.add("parametros");
        listDataChild.put(listDataHeader.get(0),heading1);

        List<String> heading2 = new ArrayList<String>();
        heading2.add("colores");
        heading2.add("datos entre fragmentos");
        heading2.add("dialog sumar");
        heading2.add("recyclerview");
        listDataChild.put(listDataHeader.get(1),heading2);

        List<String> heading8 = new ArrayList<String>();
        heading8.add("Proximidad");
        heading8.add("luz");
        heading8.add("Acelerometro");

        listDataChild.put(listDataHeader.get(2),heading8);

        List<String> heading4 = new ArrayList<String>();
        heading4.add("artistas");
        heading4.add("reyes");
        listDataChild.put(listDataHeader.get(3),heading4);

        List<String> heading5= new ArrayList<String>();
        heading5.add("SD");
        heading5.add("helper");
        heading5.add("ORM");

        listDataChild.put(listDataHeader.get(4),heading5);
        List<String> heading6= new ArrayList<String>();
        heading6.add("hilo");
        heading6.add("hilo_weather");
        heading6.add("volly");

        List<String> heading9 = new ArrayList<>();
        heading9.add("Estudiante_hilo");
        heading9.add("Estudiante_voley");
        listDataChild.put(listDataHeader.get(7),heading9);

        listDataChild.put(listDataHeader.get(5),heading6);
        List<String> heading10 = new ArrayList<>();
        heading10.add("Mapas");

        listDataChild.put(listDataHeader.get(6),heading10);

    }

        private void setupDrawerContent(NavigationView navigation){
        navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                menuItem.setChecked(true);
                drawer.closeDrawers();
                return true;
            }
        });
        }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.principal, menu);
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    /*   @Override
       public boolean onSupportNavigateUp() {
           NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
           return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                   || super.onSupportNavigateUp();
       }
   */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent intent;
        switch(item.getItemId()){
            case android.R.id.home:
                drawer.openDrawer(GravityCompat.START);
                return true;

            case R.id.opcion_login:
                intent = new Intent(this, login.class);
                startActivity(intent);
                break;
            case R.id.opcion_sumar:
                intent = new Intent(this, suma.class);
                startActivity(intent);
                break;
            case R.id.opcion_parametro:
                intent = new Intent(this, enviarDatos.class);
                startActivity(intent);
                break;
            case R.id.colores:
                intent = new Intent(this, fragmento.class);
                startActivity(intent);
                break;
            case R.id.opcion_data_fragmentos:
                intent = new Intent(this, ejercicio1.class);
                startActivity(intent);
                break;
            case R.id.opcion_dialog_sumar:
                final Dialog dlgSUmar = new Dialog(principal.this);
                dlgSUmar.setContentView(R.layout.dialog_sumar);
                Button btn_dialog_sumar = dlgSUmar.findViewById(R.id.dialog_btn_sumar);
                final EditText caja1 = dlgSUmar.findViewById(R.id.dialog_numero1);
                final EditText caja2 = dlgSUmar.findViewById(R.id.dialog_numero2);
                dlgSUmar.show();
                btn_dialog_sumar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        double numero = Double.parseDouble(caja1.getText().toString())+Double.parseDouble(caja2.getText().toString());
                        Toast.makeText(principal.this,"sumar es "+numero,Toast.LENGTH_SHORT).show();
                        dlgSUmar.hide();

                    }
                });
            case R.id.opcion_recyclerview:
                intent = new Intent(this, activity_recicler_artistas.class);
                startActivity(intent);
                break;

            case R.id.opcion_reyes:
                intent = new Intent(this, leecutra_reyes
                        .class);
                startActivity(intent);
            case R.id.opcion_actividad_artistas:
                 intent = new Intent(this,actividad_archivosArtistas.class);
                startActivity(intent);
                break;
            case R.id.opcion_helper:
                intent = new Intent(this, ActividadProductoHelper
                        .class);
                startActivity(intent);
                break;
            case R.id.opcion_ORM:
                intent = new Intent(this, AppORM.class);
                startActivity(intent);
                break;
            case R.id.opcion_hilo:
                intent = new Intent(this, servicioWebAlumno.class);
                startActivity(intent);
                break;
            case R.id.opcion_SW_estudiante_volley:
                intent= new Intent(this, estudiante.class);
                startActivity(intent);
                break;
            case R.id.opcion_SW_estudiante_hilo:
                intent = new Intent(this, Servicio_estudiante_hilo.class);
                startActivity(intent);
                break;
            case R.id.opcion_mapas:
                intent = new Intent(this, MapsActivity.class);
                startActivity(intent);
                break;
            case R.id.opcion_acelerometro:
                intent = new Intent(this, Sensores.class);
                startActivity(intent);
                break;
            case R.id.opcion_proximidad:
                intent = new Intent(this, Sensor_proximidad.class);
                startActivity(intent);
                break;
            case R.id.opcion_luz:
                intent = new Intent(this, Sensor_luz.class);
                startActivity(intent);
                break;


        }

        return super.onOptionsItemSelected(item);
    }
}
