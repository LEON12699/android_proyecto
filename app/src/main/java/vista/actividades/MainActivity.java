package vista.actividades;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.leonisrael.R;

import vista.fragmentos.fragmento;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button uno,dos ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cargar();


    }


    public void cargar(){
        uno = findViewById(R.id.btnBoton);
        dos =  findViewById(R.id.btnBoton2);
        uno.setOnClickListener(this);
        dos.setOnClickListener(this);
    }


    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        Intent intent;
        switch(v.getId()){
            case R.id.btnBoton:
                intent = new Intent(MainActivity.this,login.class);
                startActivity(intent);
                break;
            case R.id.btnBoton2:
                intent = new Intent(MainActivity.this, suma.class);
                startActivity(intent);
                break;
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // permite crear un objeto para manejear el xml
        // el metodo inflate permite agregar un menu implementado de un archivo xml a la actividad
        MenuInflater inflaterMenu = getMenuInflater();
        inflaterMenu.inflate(R.menu.main,menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // este metodod pemrite erealizar eventos en cada item hijo
        Intent intent;
        switch(item.getItemId()){
            case R.id.opcion_login:
                intent = new Intent(MainActivity.this, login.class);
                startActivity(intent);
                break;
            case R.id.opcion_sumar:
                intent = new Intent(MainActivity.this, suma.class);
                startActivity(intent);
                break;
            case R.id.opcion_fragmentos:
                intent = new Intent(MainActivity.this, fragmento.class);
                startActivity(intent);
                break;
            case R.id.opcion_dialog_sumar:
                final Dialog dlgSUmar = new Dialog(MainActivity.this);
                dlgSUmar.setContentView(R.layout.dialog_sumar);
                Button btn_dialog_sumar = dlgSUmar.findViewById(R.id.dialog_btn_sumar);
                final EditText caja1 = dlgSUmar.findViewById(R.id.dialog_numero1);
                final EditText caja2 = dlgSUmar.findViewById(R.id.dialog_numero2);
                dlgSUmar.show();
                btn_dialog_sumar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        double numero = Double.parseDouble(caja1.getText().toString())+Double.parseDouble(caja2.getText().toString());
                        Toast.makeText(MainActivity.this,"sumar es "+numero,Toast.LENGTH_SHORT).show();
                        dlgSUmar.hide();

                    }
                });

                break;
        }
        return true;
    }
}
