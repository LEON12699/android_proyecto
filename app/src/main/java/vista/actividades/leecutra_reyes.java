package vista.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.os.Bundle;

import android.view.View;

import android.widget.TextView;
import android.widget.Toast;

import com.example.leonisrael.R;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


import java.io.IOException;

import java.util.ArrayList;
import java.util.List;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import modelo.reyes;

public class leecutra_reyes extends AppCompatActivity {
    List<reyes> listarreyes;
    TextView texto;
    RecyclerView recyclerViewReyes;
    reyesAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leecutra_reyes);
        recyclerViewReyes = findViewById(R.id.recycler_reyes);
        texto = findViewById(R.id.txt_reyes);
        leer();
    }
    public void leer(){
            listarreyes = new ArrayList<reyes>();
        try {
;
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

            Document doc = builder.parse(getResources().openRawResource(R.raw.reyes),null);
            NodeList godos = doc.getElementsByTagName("godo");

            reyes rey;
            Toast.makeText(this,"numero de reyes es"+godos.getLength(),Toast.LENGTH_LONG).show();
            for (int i = 0; i <godos.getLength() ; i++) {
                //LIST
                //LIST ADD(element)godos.item(1).getContext()
                rey = new reyes();
                System.out.println(godos.item(i).getChildNodes().getLength());
                if(godos.item(i).getChildNodes().getLength()>1) {
                    Element e = (Element) godos.item(i);

                    rey.setNombre(e.getElementsByTagName("nombre").item(0).getTextContent());
                    rey.setPerido(e.getElementsByTagName("periodo").item(0).getTextContent());
                    listarreyes.add(rey);
                }



            }
            cargarRecycler();
            //cargarRecycler(cadena.split(";"),",");
        } catch (IOException | ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }


    }

    private void cargarRecycler(){
        adapter = new reyesAdapter(listarreyes, new RecyclerViewOnItemClickListener() {
            @Override
            public void onClick(View v, int posicion) {

            }
        });
        recyclerViewReyes.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration divider = new DividerItemDecoration(recyclerViewReyes.getContext(),((LinearLayoutManager)recyclerViewReyes.getLayoutManager()).getOrientation());

        recyclerViewReyes.addItemDecoration(divider);
        recyclerViewReyes.setAdapter(adapter);
    }
}
