package vista.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.leonisrael.R;

public class suma extends AppCompatActivity implements View.OnClickListener {
    EditText numero1,numero2;
    TextView respuesta;
    Button btn_sumar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suma);
        cargarCOmponentes();
    }

    private void cargarCOmponentes(){
        numero1 = findViewById(R.id.txtNumero1);
        numero2 = findViewById(R.id.txtNumero2);
        btn_sumar = findViewById(R.id.btnSuma);
        respuesta = findViewById(R.id.txt_Rta);
        btn_sumar.setOnClickListener(this);
    }


    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        int a = Integer.parseInt(numero1.getText().toString());
        int b =Integer.parseInt(numero2.getText().toString());
        int c = a+b;

        respuesta.setText(String.valueOf(c));
    }
}
