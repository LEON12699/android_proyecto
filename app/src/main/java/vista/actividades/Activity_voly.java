package vista.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.leonisrael.R;
import com.example.leonisrael.ServivioWebVollyAlumno;
import com.example.leonisrael.servicioWebAlumno;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import controlador.SingletonAlumnoVolly;
import controlador.volly_async;
import modelo.alumno;
import vista.adapter.alumnoAdapter;

public class Activity_voly extends AppCompatActivity implements View.OnClickListener {


    ServivioWebVollyAlumno servicio = new ServivioWebVollyAlumno(this);


    EditText c_nombre,c_id,c_direccion;
    RecyclerView recycler;
    TextView c_datos;
    Button btn_guardar, btn_actualizar,btn_eliminar,btn_buscarTodo,btn_Buscar_id;

    alumnoAdapter adapter;
    List<alumno> lista;
    alumno a;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voly);
        cargar();


    }

    public  void cargar(){
        c_nombre = findViewById(R.id.txtvoly_nombre_alumno);
        c_datos = findViewById(R.id.txt_voly_datos_alumnos);
        c_id = findViewById(R.id.txtvoly_id_alumno);
        c_direccion= findViewById(R.id.txtvoly_direccion_alumno);
        recycler = findViewById(R.id.recyclervoly_alumnos);
        btn_actualizar= findViewById(R.id.btnvoly_actualizar_alumno);
        btn_guardar= findViewById(R.id.btnvoly_guardar_alumno);
        btn_buscarTodo= findViewById(R.id.btnvoly_buscar_alumnos);
        btn_eliminar= findViewById(R.id.btnvoly_eliminar_alumnos);
        btn_Buscar_id=findViewById(R.id.btnvoly_obtener_id_alumno);

        btn_actualizar.setOnClickListener(this);
        btn_guardar.setOnClickListener(this);
        btn_actualizar.setOnClickListener(this);
        btn_Buscar_id.setOnClickListener(this);
        btn_eliminar.setOnClickListener(this);
        btn_buscarTodo.setOnClickListener(this);
    }

    public void Leer(JSONObject object){

        try {
            lista = new ArrayList<>();
            if (object.has("alumno")){
                JSONObject alumno = object.getJSONObject("alumno");
                alumno a = new alumno();
                a.setId(alumno.getInt("idAlumno"));
                a.setDireccion(alumno.getString("direccion"));
                a.setNombre(alumno.getString("nombre"));
                lista.add(a);
            }
            else if(object.has("alumnos")){
                JSONArray arr = object.optJSONArray("alumnos");
                //  Log.e("arreglo",arr.getString(0));
                for (int i = 0; i <arr.length() ; i++) {
                    alumno a = new alumno();
                    a.setId(arr.getJSONObject(i).getInt("idalumno"));
                    a.setDireccion(arr.getJSONObject(i).getString("direccion"));
                    a.setNombre(arr.getJSONObject(i).getString("nombre"));
                    lista.add(a);

                }
            }else
            {
                Toast.makeText(getApplicationContext(),object.getString("mensaje"),Toast.LENGTH_LONG).show();
            }
            carga_recycler();
        }catch (Exception e){
            e.printStackTrace();
            Log.e("eroor",e.toString());
        }
    }


    public void carga_recycler(){
        adapter = new alumnoAdapter(lista, new RecyclerViewOnItemClickListener() {
            @Override
            public void onClick(View v, int posicion) {
                TextView nombre =  v.findViewById(R.id.txt_recycler_nombrealumno);
                TextView direccion = v.findViewById(R.id.txt_direccionalumno);
                TextView id = v.findViewById(R.id.txt_recycler_alumnoid);

                c_nombre.setText(nombre.getText().toString());
                c_id.setText(id.getText().toString());
                c_direccion.setText(direccion.getText().toString());
            }
        });
        recycler.setLayoutManager(new LinearLayoutManager(this));

        DividerItemDecoration divider = new DividerItemDecoration(recycler.getContext(),((LinearLayoutManager)recycler.getLayoutManager()).getOrientation());
        recycler.addItemDecoration(divider);
        recycler.setAdapter(adapter);

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnvoly_buscar_alumnos:
                servicio.findAllStudents(new volly_async() {
                    @Override
                    public void response(JSONObject json) {
                       // c_datos.setText(json.toString());
                        Leer(json);
                }
                });


                break;
            case R.id.btnvoly_guardar_alumno:
                a = new alumno();
                a.setNombre(c_nombre.getText().toString());
                a.setDireccion(c_direccion.getText().toString());
                servicio.InsertStudent(a, new volly_async() {
                    @Override
                    public void response(JSONObject json) {
                        c_datos.setText(json.toString());
                        Leer(json);
                    }
                });
                break;
            case R.id.btnvoly_eliminar_alumnos:
                 a= new alumno();
                 a.setId(Integer.parseInt(c_id.getText().toString()));
                servicio.DeleteIdStudent(a, new volly_async() {
                    @Override
                    public void response(JSONObject json) {
                        Leer(json);
                    }
                });
                break;
            case R.id.btnvoly_actualizar_alumno:
                a= new alumno();
                a.setId((Integer.parseInt(c_id.getText().toString())));
                a.setNombre(c_nombre.getText().toString());
                a.setDireccion(c_direccion.getText().toString());
                servicio.UpdateStudent(a, new volly_async() {
                    @Override
                    public void response(JSONObject json) {
                        Leer(json);
                    }
                });
                break;
            case R.id.btnvoly_obtener_id_alumno:
                servicio.getByid(c_id.getText().toString(), new volly_async() {
                    @Override
                    public void response(JSONObject json) {
                        Leer(json);
                    }
                });
                break;
        }
    }
}
