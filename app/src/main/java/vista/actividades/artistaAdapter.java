package vista.actividades;


import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.example.leonisrael.R;

import java.util.List;
import java.util.Vector;

import modelo.artista;

public class artistaAdapter extends RecyclerView.Adapter<artistaAdapter.ViewHolderartista> {
     List<artista> lista;
    private RecyclerViewOnItemClickListener recyclerViewOnItemClickListener;

public artistaAdapter(List<artista> lista, RecyclerViewOnItemClickListener recyclerViewOnItemClickListener){
    this.lista = lista;
    this.recyclerViewOnItemClickListener = recyclerViewOnItemClickListener;
}




    @Override
    public ViewHolderartista onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.artistas,parent,false);
        return new ViewHolderartista(view);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolderartista holder, int position) {
       holder.dato1.setText(lista.get(position).getNombre());
        holder.dato2.setText(lista.get(position).getApellido());
        holder.dato3.setText(lista.get(position).getNombre_artistico());
        if (lista.get(position).getFoto()!= null) {
            Uri url = Uri.parse(lista.get(position).getFoto());
            Log.e("URL", url.toString());
            holder.imagen.setImageURI(url);
        }
    }


    @Override
    public int getItemCount() {
        return lista.size();
    }

    public  class ViewHolderartista extends RecyclerView.ViewHolder implements View.OnClickListener
    {   TextView dato1,dato3;
        TextView dato2;
        ImageView imagen;
        public ViewHolderartista(@NonNull View itemView) {
            super(itemView);
            dato1 = itemView.findViewById(R.id.lbl_nombre_artista);
            dato2 = itemView.findViewById(R.id.lbl_apellido_artista);
            imagen= itemView.findViewById(R.id.imagen);
            dato3 = itemView.findViewById(R.id.lbl_nombre_comercial_artista);
            itemView.setOnClickListener(this);

        }

        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        @Override
        public void onClick(View v) {
            recyclerViewOnItemClickListener.onClick(v,getAdapterPosition());
        }


    }
}
