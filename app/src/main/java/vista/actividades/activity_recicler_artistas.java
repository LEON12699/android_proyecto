package vista.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.leonisrael.R;
import com.example.leonisrael.principal;

import java.util.ArrayList;
import java.util.List;

import modelo.artista;

public class activity_recicler_artistas extends AppCompatActivity {
RecyclerView recyclerViewArtista;
artistaAdapter adapter;
List<artista> listarArtistas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recicler_artistas);
        tomarControl();
        cargarRecycler();
    }

    private void tomarControl(){
        recyclerViewArtista = findViewById(R.id.recycler_artistas);
    }
    private void cargarRecycler(){
        listarArtistas = new ArrayList<artista>();

        artista artista1 = new artista();
        artista1.setNombre("nombre");
        artista1.setApellido("apellido");
        artista1.setNombre_artistico("chayane");
        artista1.setFoto("android.resource://"+getPackageName()+"/"+R.drawable.alien);
        listarArtistas.add(artista1);

        artista artista2 = new artista();
        artista2.setNombre("nombre2");
        artista2.setApellido("apellido2");
        artista2.setNombre_artistico("donMedardo");
        artista2.setFoto("android.resource://"+getPackageName()+"/"+R.drawable.alien2);


        listarArtistas.add(artista2);
        adapter = new artistaAdapter(listarArtistas, new RecyclerViewOnItemClickListener() {
            @Override
            public void onClick(View v, int posicion) {
                TextView nombre_comercial = v.findViewById(R.id.lbl_nombre_comercial_artista);
                ImageView dato_imagen = v.findViewById(R.id.imagen);
                TextView nombre = v.findViewById(R.id.lbl_nombre_artista);
                TextView apellido = v.findViewById(R.id.lbl_apellido_artista);

                final Dialog dlgartista = new Dialog(activity_recicler_artistas.this);
                dlgartista.setTitle("ARTISTA");
                dlgartista.setContentView(R.layout.dialog_artista);

                final TextView caja1 = dlgartista.findViewById(R.id.lbl_dialog_artista);
                final TextView caja2 = dlgartista.findViewById(R.id.lbl_dialog_apellido);
                final TextView caja3 = dlgartista.findViewById(R.id.lbl_dialog_nombre);
                final ImageView imagenview = dlgartista.findViewById(R.id.imagen_dialog_artista);
                ImageButton btn = dlgartista.findViewById(R.id.btn_dialog_image);

                caja1.setText(nombre_comercial.getText());
                caja2.setText(nombre.getText());
                caja3.setText(apellido.getText());
                imagenview.setImageDrawable(dato_imagen.getDrawable());



                    dlgartista.show();
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dlgartista.hide();

                    }
                });

            }
        });
        recyclerViewArtista.setLayoutManager(new LinearLayoutManager(this));

        DividerItemDecoration divider = new DividerItemDecoration(recyclerViewArtista.getContext(),((LinearLayoutManager)recyclerViewArtista.getLayoutManager()).getOrientation());
        recyclerViewArtista.addItemDecoration(divider);
        recyclerViewArtista.setAdapter(adapter);
    }
}
