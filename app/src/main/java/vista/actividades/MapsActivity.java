package vista.actividades;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.leonisrael.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import controlador.Lector;
import modelo.Ruta;
import vista.adapter.CustomInfoAdapter;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener {

     FusedLocationProviderClient fusedLocationClient;

    private GoogleMap mMap;
    private Button btn_satelite, btn_terreno, btn_hibrido;

    public void cargar() {
        btn_hibrido = findViewById(R.id.btn_Hibrido);
        btn_satelite = findViewById(R.id.btn_satelite);
        btn_terreno = findViewById(R.id.btn_terreno);

        btn_hibrido.setOnClickListener(this);
        btn_satelite.setOnClickListener(this);
        btn_terreno.setOnClickListener(this);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        cargar();
         fusedLocationClient=LocationServices.getFusedLocationProviderClient(this);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and xswtreturned to the app.
     */


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        final List <LatLng> lista = new ArrayList<>(); // lista de LatLNG

        InputStream is = getResources().openRawResource(R.raw.ruta);
        List<Ruta> rutas = Lector.archivo(is);
        for (Ruta ruta:rutas){
            lista.add(ruta.getLatitudes());
            if (ruta.has_info()){

                Marker marker= mMap.addMarker(new MarkerOptions().
                        position(ruta.getLatitudes()).
                        title(ruta.getInfo()).
                        icon(BitmapDescriptorFactory.fromResource(R.drawable.icono))
                        .snippet(ruta.getDescripcion())

                );
                marker.setTag("Tag");
            }
        }
        mMap.addMarker(new MarkerOptions().
            position(new LatLng(-4.016118, -79.200996)).
                title("INICIA RUTA")
                .snippet("Esta es mi casa").
                icon(BitmapDescriptorFactory.fromResource(R.drawable.icono) )
               .draggable(true)
        .flat(true)
        );


        final Polyline polyline1= mMap.addPolyline(new PolylineOptions().clickable(true).width(1.5f)
        );
        /** final Polyline polyline1 = mMap.addPolyline(new PolylineOptions()
                .clickable(true)
                .add(
                        new LatLng(-35.016, 143.321),
                        new LatLng(-34.747, 145.592),
                        new LatLng(-34.747, 145.592),
                        new LatLng(-34.364, 147.891),
                        new LatLng(-33.501, 150.217),
                        new LatLng(-32.306, 149.248),
                        new LatLng(-32.491, 147.309)));

        LocationManager locManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        LocationListener locationListenerGPS=new LocationListener() {
            LatLng la;
            @Override
            public void onLocationChanged(android.location.Location location) {
                double lat=location.getLatitude();
                double lon=location.getLongitude();
                la= new LatLng(lat,lon);


            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }

        };
        // Add a marker in Sydney and move the camera
        locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10, locationListenerGPS);
    locManager.getLastKnownLocation("gps");*/



       LatLng sydney = new LatLng(-4.016118, -79.200997);

       polyline1.setPoints(lista);
        polyline1.setColor(Color.BLUE);

        //lista.add(sydney);
       /* fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {

            @Override
            public void onSuccess(Location location) {

                if (location!=null){
                    mMap.addMarker(new MarkerOptions().
                            position(new LatLng(-4.016118, -79.200996)).
                            title("Mi ubicaicon RUTA")
                            .snippet("Esta es mi ubicaicon actual")
                                    );
                    //Toast.makeText(getApplicationContext(),location.toString(),Toast.LENGTH_LONG).show();
            /*        lista.add(new LatLng(location.getLatitude(),location.getLongitude()));
                    lista.add(new LatLng(location.getLatitude()+0.05,location.getLongitude()+0.01));
                    lista.add(new LatLng(location.getLatitude()-0.03,location.getLongitude()-0.20));
                    lista.add(new LatLng(location.getLatitude()+0.21,location.getLongitude()+0.05));
                    lista.add(new LatLng(location.getLatitude()+0.121,location.getLongitude()+0.150));
                    lista.add(new LatLng(location.getLatitude(),location.getLongitude()));


                }
            }
        });

*/

        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Loja"));

        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setMyLocationEnabled(true);

        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setAllGesturesEnabled(true);
       //-----------------------------------------------------//
        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.getUiSettings().setIndoorLevelPickerEnabled(true);

// Store a data object with the polyline, used here to indicate an arbitrary type.


        polyline1.setTag("A");
        mMap.setInfoWindowAdapter(new CustomInfoAdapter(LayoutInflater.from(this)));

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,17.0f));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_satelite:
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
            case R.id.btn_terreno:
                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                break;
            case R.id.btn_Hibrido:
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                break;
        }
    }
}
