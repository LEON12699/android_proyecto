package vista.actividades;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.leonisrael.R;
import com.example.leonisrael.modelo.carro;

import java.util.List;

public class carroAdapter  extends RecyclerView.Adapter<carroAdapter.ViewHolderCarro> {
    List<carro> lista;
    private RecyclerViewOnItemClickListener recyclerViewOnItemClickListener;

    public carroAdapter(List<carro> lista, RecyclerViewOnItemClickListener recyclerViewOnItemClickListener) {
        this.lista = lista;
        this.recyclerViewOnItemClickListener = recyclerViewOnItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolderCarro onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_carro,parent,false);
        return new ViewHolderCarro(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderCarro holder, int position) {
        holder.year.setText(String.valueOf(lista.get(position).getYear()));
        holder.marca.setText(lista.get(position).getMarca());
        holder.modelo.setText(lista.get(position).getModelo());
       holder.placa.setText(lista.get(position).getPlaca());
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class ViewHolderCarro extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView year, marca,modelo,placa;



        public ViewHolderCarro(@NonNull View itemView) {
            super(itemView);
            year= itemView.findViewById(R.id.item_txt_año);
            marca = itemView.findViewById(R.id.item_txt_marca);
            modelo = itemView.findViewById(R.id.item_txt_modelo);
            placa=itemView.findViewById(R.id.item_txt_placa);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            recyclerViewOnItemClickListener.onClick(v,getAdapterPosition());
        }
    }
}
