package vista.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import android.widget.TextView;
import android.widget.Toast;

import modelo.Producto;
import controlador.HelperProducto;
import com.example.leonisrael.R;

import java.util.ArrayList;
import java.util.List;

public class ActividadProductoHelper extends AppCompatActivity implements View.OnClickListener {
    EditText descripcion,codigo,cantidad,precio;
    RecyclerView recycler;
    Button agregar, buscar,buscarcodigo,eliminarTodo,eLiminarCodigo,modificar;
    List<Producto> productos;
    HelperProducto helper = new HelperProducto(this,"ventas",null,2);
    productoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_producto_helper);
        cargar();
    }

    public void cargar(){
        descripcion = findViewById(R.id.txt_PdescripcionHelp);
        cantidad = findViewById(R.id.txt_PcantidadHelp);
        codigo = findViewById(R.id.txt_PcodigoHelp);
        precio = findViewById(R.id.txt_PprecioHelp);
        recycler = findViewById(R.id.recycler_PHelp);
        agregar = findViewById(R.id.btn_PagregarHelp);
        buscar= findViewById(R.id.btn_PbuscarHelp);
        buscarcodigo = findViewById(R.id.btn_Pbuscar_codigo);
        eliminarTodo = findViewById(R.id.btn_Peliminar);
        eLiminarCodigo =findViewById(R.id.btn_Pelimina_codigo);
        modificar=findViewById(R.id.btn_Pmodificar);

        agregar.setOnClickListener(this);
        buscar.setOnClickListener(this);
        buscarcodigo.setOnClickListener(this);
        eliminarTodo.setOnClickListener(this);
        eLiminarCodigo.setOnClickListener(this);
        modificar.setOnClickListener(this);

    }


    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_PagregarHelp:
                Producto nuevo = new Producto((Integer.parseInt(codigo.getText().toString())),
                        descripcion.getText().toString(),(Double.parseDouble(precio.getText().toString())),(Integer.parseInt(cantidad.getText().toString())));
                        helper.insertar(nuevo);
                break;
            case R.id.btn_PbuscarHelp:
                productos = new ArrayList<Producto>();

                productos=helper.getProductos();
                adapter = new productoAdapter(productos, new RecyclerViewOnItemClickListener() {
                    @Override
                    public void onClick(View v, int posicion) {
                       TextView re_precio = v.findViewById(R.id.txt_prodcutos_precio);
                        TextView re_cantidad = v.findViewById(R.id.txt_prodcutos_cantidad);
                        TextView re_codigo = v.findViewById(R.id.txt_prodcutos_codigo);
                        TextView re_descripcion = v.findViewById(R.id.txt_productos_descrcipcion);

                        codigo.setText(re_codigo.getText().toString());
                        precio.setText(re_precio.getText().toString());
                        cantidad.setText(re_cantidad.getText().toString());
                        descripcion.setText(re_descripcion.getText().toString());
                    }
                });
                recycler.setLayoutManager(new LinearLayoutManager(this));
                DividerItemDecoration divider = new DividerItemDecoration(recycler.getContext(),((LinearLayoutManager)recycler.getLayoutManager()).getOrientation());

                recycler.addItemDecoration(divider);
                recycler.setAdapter(adapter);
                break;
            case R.id.btn_Pbuscar_codigo:
                productos = new ArrayList<Producto>();
                productos = helper.buscar_codigo(codigo.getText().toString());
                adapter = new productoAdapter(productos, new RecyclerViewOnItemClickListener() {
                    @Override
                    public void onClick(View v, int posicion) {
                        Toast.makeText(v.getContext(),"funciona",Toast.LENGTH_SHORT).show();
                    }
                });
                recycler.setLayoutManager(new LinearLayoutManager(this));
                recycler.addItemDecoration( new DividerItemDecoration(recycler.getContext(),((LinearLayoutManager)recycler.getLayoutManager()).getOrientation()));
                recycler.setAdapter(adapter);
                break;
            case R.id.btn_Peliminar:
                productos = new ArrayList<Producto>();
                helper.eliminar();
                adapter = new productoAdapter(productos, new RecyclerViewOnItemClickListener() {
                    @Override
                    public void onClick(View v, int posicion) {
                        Toast.makeText(v.getContext(),"funciona",Toast.LENGTH_SHORT).show();
                    }
                });
                recycler.setLayoutManager(new LinearLayoutManager(this));
                recycler.addItemDecoration( new DividerItemDecoration(recycler.getContext(),((LinearLayoutManager)recycler.getLayoutManager()).getOrientation()));
                recycler.setAdapter(adapter);
                break;
            case R.id.btn_Pelimina_codigo:
                productos = new ArrayList<Producto>();
                Producto peliminar = new Producto((Integer.parseInt(codigo.getText().toString())),
                        descripcion.getText().toString(),(Double.parseDouble(precio.getText().toString())),(Integer.parseInt(cantidad.getText().toString())));
                        helper.eliminar(peliminar);
                        productos = helper.getProductos();
                adapter = new productoAdapter(productos, new RecyclerViewOnItemClickListener() {
                    @Override
                    public void onClick(View v, int posicion) {
                        Toast.makeText(v.getContext(),"funciona",Toast.LENGTH_SHORT).show();
                    }
                });
                recycler.setLayoutManager(new LinearLayoutManager(this));
                recycler.addItemDecoration( new DividerItemDecoration(recycler.getContext(),((LinearLayoutManager)recycler.getLayoutManager()).getOrientation()));
                recycler.setAdapter(adapter);
                break;
            case R.id.btn_Pmodificar:
                productos = new ArrayList<Producto>();
                Producto pmodificar = new Producto((Integer.parseInt(codigo.getText().toString())),
                        descripcion.getText().toString(),(Double.parseDouble(precio.getText().toString())),(Integer.parseInt(cantidad.getText().toString())));
                helper.modificar(pmodificar);
                productos = helper.getProductos();
                adapter = new productoAdapter(productos, new RecyclerViewOnItemClickListener() {
                    @Override
                    public void onClick(View v, int posicion) {

                        Toast.makeText(v.getContext(),"funciona",Toast.LENGTH_SHORT).show();
                    }
                });
                recycler.setLayoutManager(new LinearLayoutManager(this));
                recycler.addItemDecoration( new DividerItemDecoration(recycler.getContext(),((LinearLayoutManager)recycler.getLayoutManager()).getOrientation()));
                recycler.setAdapter(adapter);
                break;
        }
    }
}
