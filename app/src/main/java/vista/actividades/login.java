package vista.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.leonisrael.R;

public class login extends AppCompatActivity implements View.OnClickListener{
    private EditText nombre ,contraseña;
    private Button btnsuma;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Cargarcomponentes();
    }

    private void Cargarcomponentes(){
        nombre = findViewById(R.id.txtNombre);
        contraseña = findViewById(R.id.txtPasword);
        btnsuma = findViewById(R.id.btnLogin);
        btnsuma.setOnClickListener(this);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        Toast.makeText(login.this
                ,"usuario "+nombre.getText()+"CLAVE"+contraseña.getText(),Toast.LENGTH_SHORT).show();

    }
}
