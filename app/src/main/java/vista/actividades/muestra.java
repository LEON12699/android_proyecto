package vista.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.leonisrael.R;

public class muestra extends AppCompatActivity {
    TextView nombres,apellidos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_muestra);
        carga();
    }

    public void carga(){
        Bundle bundle = this.getIntent().getExtras();

        nombres = findViewById(R.id.label_nombre);
        apellidos = findViewById(R.id.label_apellido);
        String dato = bundle.getString("n");

        String dato2 = bundle.getString("a");
        Toast.makeText(this,dato,Toast.LENGTH_SHORT).show();
        nombres.setText("tu nombre es : "+dato);
        apellidos.setText("tu apellido es :"+dato2);
    }
}
