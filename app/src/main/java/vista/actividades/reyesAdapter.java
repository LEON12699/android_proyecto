package vista.actividades;
import modelo.reyes;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.leonisrael.R;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class reyesAdapter extends RecyclerView.Adapter<reyesAdapter.ViewHolderreyes> {
    List<reyes> lista;
    private RecyclerViewOnItemClickListener recyclerViewOnItemClickListener;

    public reyesAdapter(List<reyes> lista, RecyclerViewOnItemClickListener recyclerViewOnItemClickListener){
        this.lista = lista;
        this.recyclerViewOnItemClickListener = recyclerViewOnItemClickListener;
    }




    @Override
    public ViewHolderreyes onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.reyes,parent,false);
        return new ViewHolderreyes(view);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolderreyes holder, int position) {
        holder.dato1.setText(lista.get(position).getNombre());
        holder.dato2.setText(lista.get(position).getPerido());

    }


    @Override
    public int getItemCount() {
        return lista.size();
    }

    public  class ViewHolderreyes extends RecyclerView.ViewHolder implements View.OnClickListener
    {   EditText dato1,dato2;

        public ViewHolderreyes(@NonNull View itemView) {
            super(itemView);
            dato1 = itemView.findViewById(R.id.txt_nombre_reyes);
            dato2 = itemView.findViewById(R.id.txt_periodo_reyes);

            itemView.setOnClickListener(this);

        }

        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        @Override
        public void onClick(View v) {
            recyclerViewOnItemClickListener.onClick(v,getAdapterPosition());
        }


    }
}
