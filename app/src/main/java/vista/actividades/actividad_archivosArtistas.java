package vista.actividades;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.example.leonisrael.R;

import vista.fragmentos.Externa;
import vista.fragmentos.Frg_leer_interno;
import vista.fragmentos.Frg_memoria_interna;


public class actividad_archivosArtistas extends AppCompatActivity implements Externa.OnFragmentInteractionListener,Frg_leer_interno.OnFragmentInteractionListener,Frg_memoria_interna.OnFragmentInteractionListener {
    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.principal, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_archivos_artistas);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        FrameLayout fr = findViewById(R.id.fragmento_artistas);
        fr.removeAllViews();
        switch (item.getItemId()){

            case R.id.opcion_interno:
                Frg_memoria_interna fragmento2 = new Frg_memoria_interna();
                FragmentTransaction transaccion2 = getSupportFragmentManager().beginTransaction();
                transaccion2.replace(R.id.fragmento_artistas,fragmento2);
                transaccion2.commit();
            break;
            case R.id.opcion_lerr_interno:
                Frg_leer_interno fragmento1 = new Frg_leer_interno();
                FragmentTransaction transaccion1 = getSupportFragmentManager().beginTransaction();
                transaccion1.replace(R.id.fragmento_artistas,fragmento1);
                transaccion1.commit();
            break;
            case R.id.opcion_EXTERNA:
                Externa fragmento3 = new Externa();
                FragmentTransaction transaccion3 = getSupportFragmentManager().beginTransaction();
                transaccion3.replace(R.id.fragmento_artistas,fragmento3);
                transaccion3.commit();
            break;
        }
        return super.onOptionsItemSelected(item);
    }
}
