package vista.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.leonisrael.R;


import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import controlador.AsyncInterface;
import controlador.Clima;

public class Servicio_clima extends AppCompatActivity implements AsyncInterface {

Clima servicio = new Clima(this,this);
TextView longitud,latitud,main,descripcion,nombre,base,temp,temp_max,temp_min,humidity,pressure,deg,speed,message,country,sunrise,sunset;



    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicio_clima);
        cargar();

            servicio.execute("https://samples.openweathermap.org/data/2.5/weather?id=2172797&appid=b6907d289e10d714a6e88b30761fae22","1");
//            Log.e("a",a);



    }


    public void cargar(){
        longitud = findViewById(R.id.txt_clima_lon);
        latitud = findViewById(R.id.txt_clima_lat);
        main=findViewById(R.id.txt_clima_main);
        descripcion=findViewById(R.id.txt_clima_descripcion);
        nombre=findViewById(R.id.txt_nombre_clima);
        base= findViewById(R.id.txt_clima_base);
        temp=findViewById(R.id.txt_clima_temp);
        temp_max=findViewById(R.id.txt_clima_temp_max);
        temp_min=findViewById(R.id.txt_clima_temp_min);
        humidity=findViewById(R.id.txt_clima_humidity);
        pressure=findViewById(R.id.txt_clima_pressure);
        deg=findViewById(R.id.txt_clima_deg);
        speed=findViewById(R.id.txt_clima_spedd);
        message=findViewById(R.id.txt_clima_message);
        country=findViewById(R.id.txt_clima_country);
        sunrise=findViewById(R.id.txt_clima_sunrise);
        sunset=findViewById(R.id.txt_clima_sunset);

    }

    @Override
    public void respuesta(HashMap<String, Object> response) {
            latitud.setText(response.get("lat").toString());
        longitud.setText(response.get("lon").toString());
        main.setText(response.get("weather_main").toString());
        descripcion.setText(response.get("weather_description").toString());
        nombre.setText(response.get("name").toString());
        base.setText(response.get("base").toString());
        temp.setText(response.get("temp").toString());
        temp_max.setText(response.get("temp_max").toString());
        temp_min.setText(response.get("temp_min").toString());
        humidity.setText(response.get("humidity").toString());
        pressure.setText(response.get("pressure").toString());
        deg.setText(response.get("deg").toString());
        speed.setText(response.get("speed").toString());
        message.setText(response.get("message").toString());
        country.setText(response.get("country").toString());
        sunrise.setText(response.get("sunrise").toString());
        sunset.setText(response.get("sunset").toString());

    }
}
