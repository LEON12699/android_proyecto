package vista.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.leonisrael.R;

public class enviarDatos extends AppCompatActivity  implements View.OnClickListener{
    EditText nombre,apellido;
    Button enviar ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enviar_datos);
        cargar();
    }
    public void cargar(){
        nombre = findViewById(R.id.txt_name);
        apellido = findViewById(R.id.txt_apellido);
        enviar = findViewById(R.id.btnEnviar);
        enviar.setOnClickListener(this);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        Intent i = new Intent(this, muestra.class);
        Bundle bundle = new Bundle();
        bundle.putString("n",nombre.getText().toString());
        bundle.putString("a",apellido.getText().toString());
        i.putExtras(bundle);
        // i.putExtra("n", nombre.getText().toString());
        // i.putExtra("a", apellido.getText().toString());
        startActivity(i);
    }
}
