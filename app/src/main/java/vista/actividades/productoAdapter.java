package vista.actividades;

import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.leonisrael.R;

import java.util.List;

import modelo.Producto;

public class productoAdapter extends RecyclerView.Adapter<productoAdapter.ViewHolderproducto> {
    private List<Producto> lista;
    private RecyclerViewOnItemClickListener recyclerViewOnItemClickListener;

    public productoAdapter(List<Producto> lista, RecyclerViewOnItemClickListener recyclerViewOnItemClickListener) {
        this.lista = lista;
        this.recyclerViewOnItemClickListener = recyclerViewOnItemClickListener;
    }


    @Override
    public ViewHolderproducto onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.productos,parent,false);
        return new ViewHolderproducto(view);
    }

    @Override
    public void onBindViewHolder(@NonNull productoAdapter.ViewHolderproducto holder, int position) {
        holder.re_precio.setText(String.valueOf(lista.get(position).getPrecio()));
        holder.re_cantidad.setText(String.valueOf(lista.get(position).getCantidad()));
        holder.re_codigo.setText(String.valueOf(lista.get(position).getCodigo()));
        holder.re_descripcion.setText(String.valueOf(lista.get(position).getDescripcion()));

    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class ViewHolderproducto  extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        TextView re_precio,re_cantidad,re_codigo,re_descripcion;
        public ViewHolderproducto(@NonNull View itemView) {
            super(itemView);
           re_precio = itemView.findViewById(R.id.txt_prodcutos_precio);
            re_cantidad = itemView.findViewById(R.id.txt_prodcutos_cantidad);
            re_codigo = itemView.findViewById(R.id.txt_prodcutos_codigo);
            re_descripcion = itemView.findViewById(R.id.txt_productos_descrcipcion);
            itemView.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {

            recyclerViewOnItemClickListener.onClick(v,getAdapterPosition());
        }
    }
}