package vista.actividades;

import android.view.View;

public interface RecyclerViewOnItemClickListener {
    void onClick(View v, int posicion);
}
