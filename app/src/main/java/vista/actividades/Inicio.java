package vista.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.leonisrael.R;

public class Inicio extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
    }

   public void logear(View view){
        Intent i = new Intent(this,login.class);
        startActivity(i);
    }

    public void sumar(View view){
        Intent a = new Intent(this, suma.class);
        startActivity(a);
    }
}
