package vista.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.leonisrael.R;
import com.example.leonisrael.modelo.carro;

import java.util.ArrayList;
import java.util.List;

public class AppORM extends AppCompatActivity  implements  View.OnClickListener {
    EditText placa, marca, modelo, year;
    Button agregar, eliminar, modificar, buscarCarro, buscarPlaca,eliminaPlaca;
    TextView datos;
    RecyclerView listarCarro;
    List<carro> listacarros;
    carroAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_orm);
        cargar();

    }

    public  void cargar(){
        placa= findViewById(R.id.txt_ORM_placa);
        marca=findViewById(R.id.txt_ORM_marca);
        modelo=findViewById(R.id.txt_ORM_modelo);
        year=findViewById(R.id.txt_ORM_Year);
        agregar=findViewById(R.id.btn_ORM_Ingresar);
        eliminar=findViewById(R.id.btn_ORM_EliminarCarro);
        modificar=findViewById(R.id.btn_ORM_Modificar);
        buscarCarro=findViewById(R.id.btn_ORM_BuscarCarro);
        buscarPlaca=findViewById(R.id.btn_ORM_BuscarPlaca);
        buscarCarro.setOnClickListener(this);
        buscarPlaca.setOnClickListener(this);
        agregar.setOnClickListener(this);
        modificar.setOnClickListener(this);
        eliminar.setOnClickListener(this);
        datos = findViewById(R.id.lblDatos);
        listarCarro= findViewById(R.id.recycler_carros);
        eliminaPlaca= findViewById(R.id.btn_ORM_eliminaPlaca);
        eliminaPlaca.setOnClickListener(this);
        listacarros=new ArrayList<>();


    }

public void cargar_recycler(List<carro>lista){
adapter = new carroAdapter(lista, new RecyclerViewOnItemClickListener() {
    @Override
    public void onClick(View v, int posicion) {
        TextView imarca,ianio,imodelo,iplaca;
        imarca= v.findViewById(R.id.item_txt_marca);
        ianio =v.findViewById(R.id.item_txt_año);
        imodelo=v.findViewById(R.id.item_txt_modelo);
        iplaca=v.findViewById(R.id.item_txt_placa);
        Toast.makeText(v.getContext(),"Marca"+imarca.getText().toString()+"\n" +
                "AÑO "+ianio.getText().toString()+"\n"+
                "Modelo "+imodelo.getText().toString()+"\n"+
                "Placa "+iplaca.getText().toString(), Toast.LENGTH_LONG).show();
        marca.setText(imarca.getText().toString());
        year.setText(ianio.getText().toString());
        modelo.setText(imodelo.getText().toString());
        placa.setText(iplaca.getText().toString());
    }

});
    listarCarro.setLayoutManager(new LinearLayoutManager(this));

    DividerItemDecoration divider = new DividerItemDecoration(listarCarro.getContext(),((LinearLayoutManager)listarCarro.getLayoutManager()).getOrientation());
    listarCarro.addItemDecoration(divider);
    listarCarro.setAdapter(adapter);
}

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.btn_ORM_Ingresar:
                carro aux = new carro();
                aux.setMlaca(placa.getText().toString());
                aux.setMarca(marca.getText().toString());
                aux.setModelo(modelo.getText().toString());
                aux.setYear(Integer.parseInt(year.getText().toString()));
                aux.save();

                listacarros = carro.getAllCarro();


                cargar_recycler(listacarros);
                // save para modificar
                break;

            case R.id.btn_ORM_BuscarCarro:
                datos.setText("numero de carros " + carro.getAllCarro().size());
                listacarros = new ArrayList<>();
                listacarros =carro.getAllCarro();
                cargar_recycler(listacarros);
                break;
            case R.id.btn_ORM_EliminarCarro:
                carro.deleteAll();
                listacarros=carro.getAllCarro();
                cargar_recycler(listacarros);
            break;
            case R.id.btn_ORM_BuscarPlaca:
                carro vehiculo = carro.getCarroPlaca(placa.getText().toString());
                listacarros.clear();
                listacarros.add(vehiculo);
                cargar_recycler(listacarros);
                break;
            case R.id.btn_ORM_Modificar:
                carro.actualizar(placa.getText().toString(),modelo.getText().toString(),Integer.valueOf(year.getText().toString()),marca.getText().toString());
                listacarros= carro.getAllCarro();
                cargar_recycler(listacarros);
                break;
            case R.id.btn_ORM_eliminaPlaca:
                carro.elimina_placa(placa.getText().toString());

                Toast.makeText(this,"Eliminado carro placa "+placa.getText().toString(),Toast.LENGTH_LONG).show();
                listacarros=carro.getAllCarro();
                cargar_recycler(listacarros);
                break;
        }

    }
}
