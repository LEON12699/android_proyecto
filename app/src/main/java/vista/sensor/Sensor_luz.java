package vista.sensor;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;

import com.example.leonisrael.R;

public class Sensor_luz extends AppCompatActivity implements SensorEventListener {
    SensorManager manager;
    Sensor sensor;
    TextView x;
    ConstraintLayout contenedor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_luz);
        x = findViewById(R.id.lbl_seonsr_luz);
        manager= (SensorManager) getSystemService(SENSOR_SERVICE);
        sensor = manager.getDefaultSensor(Sensor.TYPE_LIGHT);
        contenedor = findViewById(R.id.contenedor_sensorLuz);
    }

    @Override
    protected void onResume() {
        super.onResume();
        manager.registerListener(this,sensor,manager.SENSOR_DELAY_FASTEST);

    }

    @Override
    protected void onPause() {
        super.onPause();
        manager.unregisterListener(this);

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onSensorChanged(SensorEvent event) {
        float a ;
        a = event.values[0];
        x.setText(String.valueOf(a));
        float red = a>255?255f:a;
        contenedor.setBackgroundColor(Color.argb(12,red,20.0f,10.90f));

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
