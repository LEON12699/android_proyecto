package vista.sensor;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;

import com.example.leonisrael.R;

import java.util.Collection;

public class Sensor_proximidad extends AppCompatActivity implements SensorEventListener {
    SensorManager manager;
    Sensor sensor;
    TextView x;
    ConstraintLayout contenedor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_proximidad);
        x = findViewById(R.id.lbl_sensor_proximidad);
        manager= (SensorManager) getSystemService(SENSOR_SERVICE);
        sensor = manager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        contenedor = findViewById(R.id.contenedor_senosr);
    }

    @Override
    protected void onResume() {
        super.onResume();
        manager.registerListener(this,sensor,manager.SENSOR_DELAY_FASTEST);



    }

    @Override
    protected void onPause() {
        super.onPause();
        manager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float a ;
        a = event.values[0];
        x.setText(String.valueOf(a));
        if (a>7){
            contenedor.setBackgroundColor(Color.TRANSPARENT);
        }else{
            contenedor.setBackgroundColor(Color.parseColor("#d00000"));
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
