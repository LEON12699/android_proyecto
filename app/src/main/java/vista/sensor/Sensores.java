package vista.sensor;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.leonisrael.R;
import com.example.leonisrael.principal;

public class Sensores extends AppCompatActivity implements SensorEventListener {
    SensorManager manager;
    Sensor sensor;
    TextView x,y,z;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensores);
        Toast.makeText(this, "create", Toast.LENGTH_SHORT).show();
        manager= (SensorManager) getSystemService(SENSOR_SERVICE);
        sensor = manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        cargar();
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        float a,b,c ;
        a = event.values[0];
        b = event.values[1];
        c = event.values[2];

        x.setText(String.valueOf(a*10));
        y.setText(String.valueOf(b*10));
        z.setText(String.valueOf(c*10));

    }

    @Override
    protected void onPause() {
        super.onPause();
        manager.unregisterListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        manager.registerListener(this,sensor,manager.SENSOR_DELAY_NORMAL);

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {


    }
    public void cargar(){
        x = findViewById(R.id.lbl_sensorx);
        y = findViewById(R.id.lbl_sensor_y);
        z = findViewById(R.id.lbl_sensor_z);
    }
}
