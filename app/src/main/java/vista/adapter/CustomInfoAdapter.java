package vista.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.leonisrael.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.internal.IGoogleMapDelegate;
import com.google.android.gms.maps.model.Marker;

public class CustomInfoAdapter implements GoogleMap.InfoWindowAdapter {
    private static final String TAG ="miinfo";
    private LayoutInflater inflater;

    public CustomInfoAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    @Override
    public View getInfoWindow(Marker marker) {

        if(marker.isInfoWindowShown()){
            marker.hideInfoWindow();

        }else {
            View v = inflater.inflate(R.layout.infomapa, null);

            ((TextView) v.findViewById(R.id.txt_info_titulo)).setText(marker.getTitle());
            ((TextView) v.findViewById(R.id.txt_info_descripcion)).setText(marker.getSnippet());
            ((ImageView) v.findViewById(R.id.imagen_info)).setImageResource(R.drawable.alien);

            ((ImageView) v.findViewById(R.id.imagen_info)).animate();
            return v;

        }

        return null;

    }

    @Override
    public View getInfoContents(Marker marker) {
        Log.e("contents",marker.toString());

        return null;
    }
}
