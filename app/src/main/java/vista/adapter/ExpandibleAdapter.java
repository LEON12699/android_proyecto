package vista.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.leonisrael.R;

import java.util.HashMap;
import java.util.List;

public class ExpandibleAdapter extends BaseExpandableListAdapter {
    private Context context;
    private List<ExpandedMenuModel> mListDataHeader ;//Headers
//CHILDA DATA
    private HashMap<ExpandedMenuModel,List<String>> mListDataCHild; //


    public ExpandibleAdapter(Context context, List<ExpandedMenuModel> mListDataHeader, HashMap<ExpandedMenuModel, List<String>> mListDataCHild) {
        this.context = context;
        this.mListDataHeader = mListDataHeader;
        this.mListDataCHild = mListDataCHild;

    }

    @Override
    public int getGroupCount() {
        int i = mListDataHeader.size();
        Log.d("Groupcount",String.valueOf(i));
        return this.mListDataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int childCOunt =0;
        if(groupPosition !=10){
            childCOunt=this.mListDataCHild.get(this.mListDataHeader.get(groupPosition)).size();
        }
        return childCOunt;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.mListDataHeader.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.mListDataCHild.get(this.mListDataHeader.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ExpandedMenuModel header_title =(ExpandedMenuModel)getGroup(groupPosition);
        if (convertView== null){
            LayoutInflater inflater= (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView= inflater.inflate(R.layout.listheader,null);
        }
        TextView lblListHeader = (TextView)convertView.findViewById(R.id.submenu);
        ImageView headerIcon = (ImageView)convertView.findViewById(R.id.iconimage);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(header_title.getIconName());
        headerIcon.setImageResource(header_title.getIconImg());



        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String childText = (String)getChild(groupPosition,childPosition);
        if (convertView==null){
            LayoutInflater inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView= inflater.inflate(R.layout.list_submenu,null);

        }
        TextView txtchild = convertView.findViewById(R.id.submenu2);
        txtchild.setText(childText);


        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
