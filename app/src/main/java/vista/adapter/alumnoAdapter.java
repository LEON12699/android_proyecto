package vista.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.leonisrael.R;

import java.util.List;

import modelo.alumno;
import vista.actividades.RecyclerViewOnItemClickListener;
import vista.actividades.artistaAdapter;

public class alumnoAdapter extends RecyclerView.Adapter<alumnoAdapter.ViewHolderalumno> {
    private RecyclerViewOnItemClickListener recyclerViewOnItemClickListener;
    private List<alumno> alumnoList;

    public alumnoAdapter( List<alumno> alumnoList,RecyclerViewOnItemClickListener recyclerViewOnItemClickListener) {
        this.recyclerViewOnItemClickListener = recyclerViewOnItemClickListener;
        this.alumnoList = alumnoList;
    }

    @NonNull
    @Override
    public ViewHolderalumno onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.alumnos,parent,false);
        return new alumnoAdapter.ViewHolderalumno(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderalumno holder, int position) {
        holder.nombre.setText(alumnoList.get(position).getNombre());
        holder.id.setText(String.valueOf(alumnoList.get(position).getId()));
        holder.direccion.setText(alumnoList.get(position).getDireccion());

    }

    @Override
    public int getItemCount() {
        return alumnoList.size();
    }

    public class ViewHolderalumno extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView nombre,direccion,id;
        public ViewHolderalumno(@NonNull View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.txt_recycler_alumnoid);
            direccion= itemView.findViewById(R.id.txt_direccionalumno);
            nombre =itemView.findViewById(R.id.txt_recycler_nombrealumno);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            recyclerViewOnItemClickListener.onClick(v,getAdapterPosition());
        }
    }
}
