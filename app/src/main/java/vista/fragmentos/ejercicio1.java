package vista.fragmentos;

import androidx.appcompat.app.AppCompatActivity;

import androidx.fragment.app.FragmentManager;

import android.net.Uri;
import android.os.Bundle;

import com.example.leonisrael.R;

public class ejercicio1 extends AppCompatActivity implements Frg_ejercicio1.OnFragmentInteractionListener, Frg1_ejercicio1.OnFragmentInteractionListener , Comunicador {

static int clicks=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio1);
        cargar();
    }


    public void cargar(){

       /**Frg_ejercicio1 fragmento1 = new Frg_ejercicio1() ;
        FragmentTransaction transaccion2 =   getSupportFragmentManager().beginTransaction();
        transaccion2.replace(R.id.contenedor_Frg1,fragmento1);
        transaccion2.commit();

        Frg1_ejercicio1 fragmento2 = new Frg1_ejercicio1() ;
        FragmentTransaction transaccion1 = getSupportFragmentManager().beginTransaction();
        transaccion1.replace(R.id.contenedor_frg,fragmento2);
        transaccion1.commit();**/
    }


    @Override
    public void responder(String datos) {
        FragmentManager manager = getSupportFragmentManager();
        Frg_ejercicio1 recibe = (Frg_ejercicio1) manager.findFragmentById(R.id.fragmento_enviar);
        recibe.cambiarTexto(datos);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
