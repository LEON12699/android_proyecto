package vista.fragmentos;

public interface Comunicador {
    void responder(String datos);
}
